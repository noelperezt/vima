"""
 Formularios de la aplicación Administrador.
"""
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import EmailInput, TextInput
from django.utils.translation import ugettext_lazy as _
from apps.administrador.models import ConfiguracionEmpresa


class UserCreateForm(forms.ModelForm):
    """
        Formulario para creación de usuario.
    """
    password1 = forms.CharField(
        label=_('Contraseña'),
        widget=forms.PasswordInput(
            attrs={'class': 'form-control',
                   'nombre_etiqueta': 'Contraseña'}))
    password2 = forms.CharField(
        label=_('Repetir Contraseña'),
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'nombre_etiqueta': 'Repetir Contraseña'
        }))

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username")
        widgets = {
            'first_name':
            TextInput(attrs={
                'class': 'form-control',
                'required': 'required',
                'nombre_etiqueta': 'Nombre'
            }),
            'last_name':
            TextInput(attrs={
                'class': 'form-control',
                'required': 'required',
                'nombre_etiqueta': 'Apellidos'
            }),
            'email':
            EmailInput(attrs={
                'class': 'form-control',
                'required': 'required',
                'nombre_etiqueta': 'Correo Electrónico'
            }),
            'username':
            TextInput(attrs={
                'class': 'form-control',
                'nombre_etiqueta': 'Nombre de Usuario'
            }),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo == 'email':
                self.fields[campo].label = _('Correo Electrónico')
            if campo == 'username':
                self.fields[campo].help_text = None

    def clean_password2(self):
        """Validación de contraseñas iguales."""
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            msg = _('Las contraseñas no coinciden')
            raise forms.ValidationError(msg)
        return password2

    def save(self, commit=True):
        """Redefinición de método ``save()`` para guardar la contraseña."""
        try:
            user = super(UserCreateForm, self).save(commit=False)
            user.set_password(self.cleaned_data["password1"])
        except ValidationError:
            msg = _('No se pudo guardar el Usuario')
            raise forms.ValidationError(msg)
        if commit:
            user.save()
        return user


class UserUpdateForm(forms.ModelForm):
    """
        Formulario para actualización de usuario.
    """

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username", "is_active")
        widgets = {
            'first_name':
            TextInput(attrs={'class': 'form-control',
                             'required': 'required'}),
            'last_name':
            TextInput(attrs={'class': 'form-control',
                             'required': 'required'}),
            'email':
            EmailInput(attrs={'class': 'form-control',
                              'required': 'required'}),
            'username':
            TextInput(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        """Redefinición  de ``__init__()`` para propiedades adicionales."""
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo == 'email':
                self.fields[campo].label = _('Correo Electrónico')
            if campo == 'username':
                self.fields[campo].help_text = None
            if campo == 'is_active':
                self.fields[campo].help_text = None


class EmpresaForm(forms.ModelForm):
    class Meta:
        model = ConfiguracionEmpresa
        fields = ("nombre", "direccion_fiscal", "pais_direccion_fiscal",
                  "ciudad_direccion_fiscal", "zip_direccion_fiscal",
                  "direccion_envio", "pais_direccion_envio",
                  "ciudad_direccion_envio", "zip_direccion_envio",
                  "dni_rif_documento", "correo_electronico", "logo")
        widgets = {
            'nombre':
            TextInput(attrs={'class': 'form-control'}),
            'direccion_fiscal':
            forms.Textarea(attrs={
                'class': 'form-control',
                'cols': '20',
                'rows': '5',
                'style': 'resize: none;'
            }),
            'pais_direccion_fiscal':
            forms.Select(attrs={'class': 'form-control'}),
            'ciudad_direccion_fiscal':
            forms.Select(attrs={'class': 'form-control'}),
            'zip_direccion_fiscal':
            TextInput(attrs={'class': 'form-control', 'style': 'margin: 0 !important;'}),
            'direccion_envio':
            forms.Textarea(attrs={
                'class': 'form-control',
                'cols': '20',
                'rows': '5',
                'style': 'resize: none;'
            }),
            'pais_direccion_envio':
            forms.Select(attrs={'class': 'form-control'}),
            'ciudad_direccion_envio':
            forms.Select(attrs={'class': 'form-control'}),
            'zip_direccion_envio':
            TextInput(attrs={'class': 'form-control', 'style': 'margin: 0 !important;'}),
            'dni_rif_documento':
            TextInput(attrs={'class': 'form-control'}),
            'correo_electronico':
            EmailInput(attrs={'class': 'form-control'}),
            'logo':
            forms.FileInput(attrs={'class': 'file-upload',
                       'accept': 'image/*',
                       'onchange': 'readURL(this);',
                       'nombre_etiqueta':'logo'})
        }
