from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from apps.cuentas_pagar.models import Paises, Ciudades


def get_full_name(self):
    return '{} {}'.format(self.first_name, self.last_name)


User.add_to_class('__str__', get_full_name)


class Banners(models.Model):
    nombre_archivo = models.CharField(
        max_length=250, verbose_name=_('Nombre archivo'), unique=True)
    nombre_cifrado = models.CharField(
        max_length=250, verbose_name=_('Nombre cifrado'), unique=True)
    path = models.CharField(max_length=250, null=False, blank=False)
    estado = models.BooleanField(default=True, verbose_name=_('Estado'))

    class Meta:
        db_table = 'banners'
        verbose_name = 'Banner'
        verbose_name_plural = 'Banners'


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


class ConfiguracionEmpresa(SingletonModel):
    nombre = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('Nombre de la empresa'))
    direccion_fiscal = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('dirección fiscal'))
    pais_direccion_fiscal = models.ForeignKey(
        Paises,
        related_name='pais_direccion_fiscal_emp',
        null=False,
        blank=False,
        default='',
        verbose_name=_('país'))
    ciudad_direccion_fiscal = models.ForeignKey(
        Ciudades,
        related_name='ciudad_direccion_fiscal_emp',
        null=False,
        blank=False,
        default=0,
        verbose_name=_('ciudad'))
    zip_direccion_fiscal = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('código postal'))
    direccion_envio = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('direccion para envíos'))
    pais_direccion_envio = models.ForeignKey(
        Paises,
        related_name='pais_direccion_envio_emp',
        null=False,
        blank=False,
        default='',
        verbose_name=_('país'))
    ciudad_direccion_envio = models.ForeignKey(
        Ciudades,
        related_name='ciudad_direccion_envio_emp',
        null=False,
        blank=False,
        default=0,
        verbose_name=_('ciudad'))
    zip_direccion_envio = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('código postal'))
    dni_rif_documento = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('numero de documento'))
    correo_electronico = models.EmailField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('correo electrónico'))
    logo = models.FileField(
        upload_to='logo_empresa/',
        null=True,
        verbose_name=_('logo de la empresa'))

    class Meta:
        db_table = 'configuracion_empresa'
        verbose_name = 'configuración de la empresa'