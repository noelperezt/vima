from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from apps.cuentas_pagar.views import get_cities_by_countryid

from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^iniciar_sesion/$', iniciar_sesion),
    url(r'^cerrar_sesion/$', cerrar_sesion, name='cerrar_sesion'),
    url(r'^usuario/registro/$', login_required(Usuario_Registro.as_view()), name='usuario_crear'),
    url(r'^usuario/modificar/(?P<pk>\d+)/$',
        login_required(Usuario_Modificacion.as_view()), name='usuario_modificar'),
    url(r'^usuario/listado', login_required(Usuario_Listado.as_view()), name='usuario_listado'),
    url(r'^usuario/eliminar/(?P<pk>\d+)/$',
        login_required(Usuario_Eliminacion.as_view()), name='usuario_eliminar'),
    url(r'^empresa/$', Empresa.as_view(), name='empresa'),
    url(r'^banners/$', Banners, name='banners'),
     url(r'^get_cities/$', get_cities_by_countryid, name='obtener_ciudades'),
#ESTADISTICAS
    url(r'^estadventas/(?P<anno>[^/]+)/(?P<mes>\d+)$',
        login_required(EstadisticasVentas),
        name='estadistica_ventas'),
    url(r'^estadregistros/(?P<anno>[^/]+)/(?P<mes>\d+)$',
        login_required(EstadisticasRegistros),
        name='estadistica_registros'),
    url(r'^estadcategorias/(?P<anno>[^/]+)/(?P<mes>\d+)$',
        login_required(EstadisticasCategorias),
        name='estadistica_registros'),
]
