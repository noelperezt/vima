import json

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count
from django.db.models.functions import ExtractMonth, ExtractYear
from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.shortcuts import HttpResponseRedirect, redirect, render
from django.utils.dateparse import parse_date
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from apps.administrador.forms import EmpresaForm, UserCreateForm, UserUpdateForm
from apps.administrador.models import ConfiguracionEmpresa
from apps.inventario.models import Categorias
from apps.website.models import Cliente, DetalleVentas, Ventas

# Create your views here.


def index(request):
    if request.user.is_authenticated():
        return render(request, 'base.html')
    else:
        if request.method == 'POST':
            if iniciar_sesion(request):
                if request.GET:
                    return HttpResponseRedirect(request.GET.get('next'))
                else:
                    return render(request, 'base.html')
            else:
                return render(request, 'usuario_invalido.html')
    return render(request, 'inicio_sesion.html')


def iniciar_sesion(request):
    """Validación de datos de ingreso. Permite solo usuarios con ``is_staff = True``"""
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            if user.is_staff:
                login(request, user)
                return True
            else:
                return False
        else:
            return False


def cerrar_sesion(request):
    logout(request)
    return redirect('/administrador/')


class Usuario_Registro(CreateView):
    model = User
    form_class = UserCreateForm
    template_name = 'usuarios_crear.html'
    success_url = reverse_lazy('administrador:usuario_listado')

    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.is_staff = True
        usuario.save()
        return HttpResponseRedirect(
            reverse_lazy('administrador:usuario_listado'))


class Usuario_Modificacion(UpdateView):
    model = User
    form_class = UserUpdateForm
    template_name = 'usuarios_modificar.html'
    success_url = reverse_lazy('administrador:usuario_listado')


class Usuario_Listado(ListView):
    model = User
    template_name = 'usuarios_listado.html'
    paginate_by = 20

    def get_queryset(self):
        return User.objects.all().order_by('id')


class Usuario_Eliminacion(DeleteView):
    model = User
    template_name = 'usuarios_eliminar.html'
    success_url = reverse_lazy('administrador:usuario_listado')


def EstadisticasVentas(request, anno, mes):
    fecha1 = parse_date(anno + '-01-01')
    fecha2 = parse_date(anno + '-12-31')
    V = Ventas.objects.filter(
        fecha__gte=fecha1,
        fecha__lte=fecha2).exclude(estado='Pendiente').annotate(
            month=ExtractMonth('fecha')).values('month').annotate(
                count=Count('id_venta')).values('month', 'count')
    meses = []
    cantidades = []
    for ve in V:
        if mes == '0' or mes == str(ve['month']):
            meses.append(ve['month'])
            cantidades.append(ve['count'])

    return HttpResponse(
        json.dumps({
            'mes': meses,
            'cantidad': cantidades
        }),
        content_type="application/json")


def EstadisticasRegistros(request, anno, mes):
    fecha1 = parse_date(anno + '-01-01')
    fecha2 = parse_date(anno + '-12-31')
    V = Cliente.objects.filter(
        fecha_registro__gte=fecha1, fecha_registro__lte=fecha2).annotate(
            month=ExtractMonth('fecha_registro')).values('month').annotate(
                count=Count('id_cliente')).values('month', 'count')
    meses = []
    cantidades = []
    for ve in V:
        if mes == '0' or mes == str(ve['month']):
            meses.append(ve['month'])
            cantidades.append(ve['count'])

    return HttpResponse(
        json.dumps({
            'mes': meses,
            'cantidad': cantidades
        }),
        content_type="application/json")


def EstadisticasCategorias(request, anno, mes):
    cat = Categorias.objects.filter(id_categoria_padre=1)
    dic = {}
    hijos = []
    categorias = []
    cantidades = []
    fecha1 = parse_date(anno + '-01-01')
    fecha2 = parse_date(anno + '-12-31')
    query = DetalleVentas.objects.filter(
        id_venta__fecha__gte=fecha1, id_venta__fecha__lte=fecha2).exclude(
            id_venta_id__estado="Pendiente").query
    query.order_by = ['id_venta']
    listado = QuerySet(query=query, model=DetalleVentas)

    for c in cat:
        cates = Categorias.get_all_children(
            Categorias.objects.get(descripcion=c))
        for t in cates:
            hijos.append(t.descripcion)
        dic.update({c.descripcion: {'hijos': hijos, 'cant': 0}})
        hijos = []
    contador = 0
    for d, v in dic.items():
        for l in listado:
            if l.id_producto.id_categoria.descripcion in v['hijos'] and (
                    mes == '0' or mes == str(l.id_venta.fecha.month)):
                contador = contador + 1
        categorias.append(d)
        cantidades.append(contador)
        contador = 0

    return HttpResponse(
        json.dumps({
            'categorias': categorias,
            'cantidad': cantidades
        }),
        content_type="application/json")


class Empresa(UpdateView):
    template_name = "empresa.html"
    model = ConfiguracionEmpresa
    form_class = EmpresaForm
    success_url = reverse_lazy('administrador:empresa')
    context_object_name = 'configuracion'

    def get_object(self):
        return ConfiguracionEmpresa.load()

def Banners(request):
    if request.method == "POST":
        pass

    return render(request, 'banners.html',)
