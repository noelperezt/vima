from django import forms
from apps.compras.models import OrdenesCompras, RecepcionCompras,Proveedores,Almacenes
from django.core.exceptions import ValidationError
from django.db.models import Q


class OrdenesForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['id_proveedor'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '98%', 'required': 'required', 'title': 'Proveedores', })
        self.fields['id_almacen'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '98%', 'title': 'Almacen', 'required': 'required', })
    caracteristicas = forms.HiddenInput()
    id_proveedor = forms.ModelChoiceField(queryset=Proveedores.objects.filter(activo=True))
    id_almacen = forms.ModelChoiceField(queryset=Almacenes.objects.filter(activo=True))
    caracteristicas = forms.HiddenInput()
    class Meta:
        model = OrdenesCompras
        fields = ['observaciones', 'id_almacen',
                  'id_proveedor', 'id_usuario', 'estado']
        widgets = {
            'observaciones': forms.Textarea(attrs={'class': 'form-control ', 'rows': 8, 'cols': 200, 'style': 'resize:none;'}),
            'fecha_registro': forms.HiddenInput(),
            'id_usuario': forms.HiddenInput(),
            'estado': forms.HiddenInput(),
        }


class RecepcionForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['id_orden_compra'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '70%', 'required': 'required', 'title': 'Orden de Compra', 'style': 'float:left;'})
    caracteristicas = forms.HiddenInput()
    id_orden_compra = forms.ModelChoiceField(queryset=OrdenesCompras.objects.filter(Q(estado="Enviada") | Q(estado= "Parcial")))
    class Meta:
        model = RecepcionCompras
        fields = ['id_orden_compra','fecha_recepcion','observaciones', 'monto', 'porcentaje_impuesto',
                  'impuesto', 'porcentaje_flete', 'monto_flete', 'estado']
        widgets = {
            'fecha_recepcion': forms.DateInput(attrs={'class':'form-control datepicker','date-date-format': 'YYYY-MM-DD', 'style':'    width: 100%', 'required': True}),
            'observaciones': forms.Textarea(attrs={'class': 'form-control ', 'rows': 4, 'cols': 300, 'style': 'resize:none;padding-top:14px'}),
            'monto': forms.TextInput(attrs={'onKeyPress':'return soloNumeros(event);','style':'width:100%'}),
            'porcentaje_impuesto': forms.NumberInput(attrs={'minlength': 1, 'maxlength': 100, 'required': True, 'type': 'number','style':'    width: 100%', 'onKeyPress':'return soloNumeros(event);','onfocusout':'calculoImpuesto();'}),
            'impuesto': forms.TextInput(attrs={'style':'    width: 100%', 'onKeyPress':'return soloNumeros(event);'}),
            'porcentaje_flete': forms.NumberInput(attrs={'minlength': 1, 'maxlength': 100, 'required': True, 'type': 'number','style':'    width: 100%', 'onKeyPress':'return soloNumeros(event);','onfocusout':'calculoFlete();'}),
            'monto_flete': forms.TextInput(attrs={'style':'    width: 103%', 'onKeyPress':'return soloNumeros(event);'}),
            'fecha_registro': forms.HiddenInput(),
            'id_usuario': forms.HiddenInput(),
            'estado': forms.HiddenInput(),
        }
