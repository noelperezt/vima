from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from apps.cuentas_pagar.models import Proveedores
from apps.inventario.models import Productos, Almacenes
from apps.website.models import Ventas

class OrdenesCompras(models.Model):
    id_compra = models.AutoField(primary_key=True)
    fecha_registro = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('Fecha de la Orden'))
    observaciones = models.CharField(
        max_length=300, null=True, blank=True, verbose_name=_('Observaciones'))
    monto_total = models.DecimalField(
        null=True, blank=True, max_digits=19, decimal_places=3,
                                default=0.0, verbose_name=_('monto de la orden'))
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('Usuario que Registra la Orden'))
    estado = models.CharField(
        max_length=10, null=False, blank=False, verbose_name=_('Estado de la Orden'))
    id_almacen = models.ForeignKey(
        Almacenes, null=False, blank=False, default=0, verbose_name=_('Almacén que recibirá la Mercancía'))
    id_proveedor = models.ForeignKey(
        Proveedores, null=False, blank=False, default=0, verbose_name=_('Proveedor Relacionado'))

    class Meta:
        db_table = 'orden_compra'
        verbose_name = 'Orden de Compra'
        verbose_name_plural = 'Ordenes de Compras'

    def __str__(self):
        return 'Orden #{} - Proveedor:{}'.format(self.id_compra,self.id_proveedor)


class DetalleOrdenCompra(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    id_compra = models.ForeignKey(
        OrdenesCompras, null=True, blank=True, verbose_name=_('Orden de Compra Asociada'))
    id_producto = models.ForeignKey(
        Productos, null=True, blank=True, verbose_name=_('producto'))
    talla = models.CharField(
        max_length=20, null=False, blank=True, verbose_name=_('Talla'))
    nombre_color = models.CharField(
        max_length=100, null=False, blank=True, verbose_name=_('nombre del color'))
    color = models.CharField(
        max_length=10, null=False, blank=True, verbose_name=_('color'))
    cantidad = models.IntegerField(null=True, blank=True,verbose_name=_('cantidad requerida'))
    cantidad_recibida = models.IntegerField(default=0,
                                            verbose_name=_('cantidad recibida'))
    precio = models.DecimalField(null=True, blank=True, max_digits=19, decimal_places=3,
                                 default=0.0, verbose_name=_('Precio del Producto'))
    porcentaje_ganancia = models.IntegerField(
        null=False, default = 0, verbose_name=_('porcentaje de ganancia'))
    total = models.DecimalField(null=True, blank=True, max_digits=19, decimal_places=3,
                                default=0.0, verbose_name=_('Total'))

    class Meta:
        unique_together = (("id_compra", "id_producto", "talla", "color"),)
        db_table = 'detalle_orden_compra'
        verbose_name = 'Detalle de Orden de Compra'
        verbose_name_plural = 'Detalles de Ordenes de Compras'



class RecepcionCompras(models.Model):
    id_recepcion = models.AutoField(primary_key=True)
    id_orden_compra = models.ForeignKey(
        OrdenesCompras, null=True, blank=True, verbose_name=_('Orden de Compra Asociada'))
    fecha_registro = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('Fecha de Registro'))
    fecha_recepcion = models.DateField(verbose_name=_('Fecha de Recepción'))
    observaciones = models.CharField(
        max_length=300, null=False, blank=False, verbose_name=_('Observaciones'))
    monto = models.DecimalField(max_digits=19, decimal_places=3,
                                default=0.0, verbose_name=_('Monto de la Recepción'))
    impuesto = models.DecimalField(max_digits=19, decimal_places=3,
                                   default=0.0, verbose_name=_('Monto de Impuesto'))
    porcentaje_impuesto = models.IntegerField(default=12, verbose_name=_('Porcentaje de Impuesto'))
    monto_flete = models.DecimalField(max_digits=19, decimal_places=3,
                                      default=0.0, verbose_name=_('Monto por Flete'))
    porcentaje_flete = models.IntegerField(default=10,verbose_name=_('Porcentaje por Flete'))
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('Usuario que Recibe la Mercancía'))
    estado = models.CharField(
        max_length=10, null=False, blank=False, verbose_name=_('Estado de la Recepción'))

    class Meta:
        db_table = 'recepcion_compra'
        verbose_name = 'Recepción de Compra'
        verbose_name_plural = 'Recepciones de Compras'


class DetalleRecepcionCompra(models.Model):
    id_detalle = models.AutoField(primary_key=True)
    id_producto = models.ForeignKey(
        Productos, null=True, blank=True, verbose_name=_('Producto'))
    id_recepcion = models.ForeignKey(
        RecepcionCompras, null=True, blank=True, verbose_name=_('Recepción de Compra Asociada'))
    talla = models.CharField(
        max_length=20, null=False, blank=True, verbose_name=_('Talla'))
    nombre_color = models.CharField(
        max_length=100, null=False, blank=True, verbose_name=_('nombre del Color'))
    color = models.CharField(
        max_length=10, null=False, blank=True, verbose_name=_('Color'))
    id_lote = models.IntegerField(
        null=False, verbose_name=_('Identificador del Lote'))
    cantidad = models.IntegerField(verbose_name=_('Cantidad Requerida'))
    cantidad_recibida = models.IntegerField(verbose_name=_('Cantidad Recibida'))
    precio = models.DecimalField(max_digits=19, decimal_places=3,
                                 default=0.0, verbose_name=_('Precio del Producto'))
    total = models.DecimalField(max_digits=19, decimal_places=3,
                                default=0.0, verbose_name=_('Total'))

    class Meta:
        db_table = 'detalle_recepcion_compra'
        verbose_name = 'Detalle de Recepción de Compra'
        verbose_name_plural = 'Detalles de Recepciones de Compras'

    