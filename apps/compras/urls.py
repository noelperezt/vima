from django.conf.urls import url
from .views import *
from django.contrib.auth.decorators import login_required

urlpatterns = [
    # URLs de Ordenes de compra
    url(r'^orden/search/$', search, name='search'),
    url(r'^orden/registro/$', login_required(Ordenes_Crear),name='ordenes_crear'),
    url(r'^orden/listado', login_required(Ordenes_Listado.as_view()),
        name='ordenes_listado'),
    url(r'^orden/anular/(?P<id_compra>\d+)/$',
        login_required(Orden_Anular), name='ordenes_anular'),
    url(r'^orden/modificar/(?P<id_compra>\d+)/$',
        login_required(Orden_modificar), name='ordenes_modificar'),
    url(r'^orden/previsualizar/$',
        login_required(Orden_Previsualizar), name='ordenes_previsualizar'),
    url(r'^orden/imprimir/(?P<id_compra>\d+)/', login_required(mostrar_pdf),
        name='ordenes_imprimir'),
    url(r'^orden/descargar/(?P<id_compra>\d+)/', login_required(descargar_pdf),
        name='ordenes_descargar'),
    url(r'^orden/correo/$', login_required(enviar_correo),
        name='ordenes_descargar'),
    # URLs de Recepciones de compra
    url(r'^recepcion/registro/$', login_required(Recepcion_Crear),name='recepcion_crear'),
    url(r'^recepcion/listado', login_required(Recepcion_Listado.as_view()),
        name='recepcion_listado'),
    url(r'^recepcion/anular/(?P<id_recepcion>\d+)/$',
        login_required(Recepcion_Anular), name='recepcion_anular'),
    url(r'^recepcion/detalle/$',
        login_required(Recepcion_Detalle), name='recepcion_detalle'),
    url(r'^recepcion/mostrar/$',
        login_required(Recepcion_Mostrar), name='recepcion_mostrar'),
]
