from django.http import HttpResponseBadRequest, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.core import serializers
from apps.compras.models import OrdenesCompras, DetalleOrdenCompra, RecepcionCompras, DetalleRecepcionCompra
from apps.inventario.models import Productos, Almacenes, ProductosAlmacen
from apps.cuentas_pagar.models import Proveedores, CuentasPagar
from django.contrib.auth.models import User
from apps.compras.forms import OrdenesForm,RecepcionForm
from django.views.generic import ListView, UpdateView
from django.db.models import Q
from django.shortcuts import render, redirect
import json
from django.core.mail import EmailMessage
from email.mime.base import MIMEBase
from email import encoders
from django.views.decorators.csrf import csrf_exempt
from decimal import Decimal
from apps.compras.Clases.report import JasperReportDescargar, JasperReportMostrar
import os
from django.conf import settings
import time
from django.db import transaction, IntegrityError
# Create your views here.


def search(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    buscador = request.POST['buscador']
    proveedor = request.POST['proveedor']

    palabras = buscador.split('%20')
    concat = None
    for palabra in palabras:
        if concat is None:
            concat = Q(descripcion__icontains=palabra)
        else:
            concat = concat & Q(descripcion__icontains=palabra)

    prod = Productos.objects.filter(concat).filter(id_proveedor=proveedor)

    prod_fields = (
        'descripcion',
        'precio_proveedor',
        'atributos',
    )

    data = serializers.serialize('json', prod, fields=prod_fields)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def Ordenes_Crear(request):
    if request.is_ajax():
        total=0
        prov = Proveedores.objects.get(
            id_proveedor=int(request.POST['proveedor']))
        alm = Almacenes.objects.get(id_almacen=int(request.POST['almacen']))
        usu = User.objects.get(id=int(request.POST['usuario']))
        try:
            with transaction.atomic():
                orden = OrdenesCompras(id_proveedor=prov, id_usuario=usu, id_almacen=alm, observaciones=request.POST[
                                       'observacion'], estado=request.POST['estado'])
                orden.save()
                for indx in range(int(request.POST['cantidad_detalle'])):
                    producto = 'detalle[' + str(indx) + '][codigo_producto]'
                    cant = request.POST['detalle[' + str(indx) + '][cantidad]']
                    prec = request.POST['detalle[' + str(indx) + '][precio]']
                    porc = request.POST['detalle[' + str(indx) + '][porcentaje]']
                    ta = request.POST['detalle[' + str(indx) + '][talla]']
                    ncol = request.POST['detalle[' + str(indx) + '][nombre_color]']
                    col = request.POST['detalle[' + str(indx) + '][color]']
                    prod = Productos.objects.get(
                        id_producto=int(request.POST[producto]))
                    detalle = DetalleOrdenCompra(id_compra=orden, id_producto=prod, cantidad=int(
                        cant), precio=Decimal(prec), porcentaje_ganancia=int(porc), talla=ta, color=col, nombre_color=ncol, total=int(cant) * Decimal(prec))
                    total=total + (int(cant) * Decimal(prec))
                    detalle.save()
                orden.monto_total=total
                orden.save()
                if request.POST['estado'] == "Sin Enviar":
                    return HttpResponse(json.dumps({'mensaje': 'Orden guardada satisfactoriamente','id':0}), content_type="application/json")
                else:
                    return HttpResponse(json.dumps({'mensaje': 'Orden Procesada satisfactoriamente. El proveedor recibirá ésta orden por correo electrónico.','id':orden.id_compra,'correo':prov.correo}), content_type="application/json")
        except IntegrityError:
            form.addError(prov + ' No se pudo guardar la orden.')

    return render(request, 'ordenes_crear.html', {'form': OrdenesForm})


class Ordenes_Listado(ListView):
    model = OrdenesCompras
    template_name = 'ordenes_listar.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = OrdenesCompras.objects.all().order_by('-id_compra','-estado')
        return queryset


def Orden_Anular(request, id_compra):
    orden = OrdenesCompras.objects.get(id_compra=id_compra)

    if request.method == 'POST':
        orden.estado = 'Anulada'
        orden.save()
        return redirect('compras:ordenes_listado')

    return render(request, 'ordenes_anular.html', {'orden': orden})


@csrf_exempt
def Orden_modificar(request, id_compra):
    if request.is_ajax():
        orden = OrdenesCompras.objects.get(id_compra=id_compra)
        total=0
        prov = Proveedores.objects.get(
            id_proveedor=request.POST['proveedor'])
        alm = Almacenes.objects.get(id_almacen=request.POST['almacen'])
        usu = User.objects.get(id=request.POST['usuario'])
        try:
            with transaction.atomic():
                orden.id_proveedor = prov
                orden.id_usuario=usu
                orden.id_almacen=alm
                orden.observaciones=request.POST['observacion']
                orden.estado=request.POST['estado']
                orden.save()
                DetalleOrdenCompra.objects.filter(id_compra=id_compra).delete()
                for indx in range(int(request.POST['cantidad_detalle'])):
                    producto = 'detalle[' + str(indx) + '][codigo_producto]'
                    cant = request.POST['detalle[' + str(indx) + '][cantidad]']
                    prec = str(request.POST['detalle[' + str(indx) + '][precio]'])
                    porc = request.POST['detalle[' + str(indx) + '][porcentaje]']
                    ta = request.POST['detalle[' + str(indx) + '][talla]']
                    ncol = request.POST['detalle[' + str(indx) + '][nombre_color]']
                    col = request.POST['detalle[' + str(indx) + '][color]']
                    prod = Productos.objects.get(
                        id_producto=int(request.POST[producto]))
                    detalle = DetalleOrdenCompra(id_compra=orden, id_producto=prod, cantidad=int(
                        cant), precio=Decimal(prec), porcentaje_ganancia=Decimal(porc), talla=ta, color=col, nombre_color=ncol, total=int(cant) * Decimal(prec))
                    total=total + (int(cant) * Decimal(prec))
                    detalle.save()
                orden.monto_total=total
                orden.save()

                if request.POST['estado'] == "Sin Enviar":
                    return HttpResponse(json.dumps({'mensaje': 'Orden guardada satisfactoriamente','id':0}), content_type="application/json")
                else:
                    return HttpResponse(json.dumps({'mensaje': 'Orden Procesada satisfactoriamente. El proveedor recibirá ésta orden por correo electrónico.','id':orden.id_compra,'correo':prov.correo}), content_type="application/json")
        except IntegrityError:
            form.addError(prov + ' No se pudo guardar la orden.')

    else:
        instance = OrdenesCompras.objects.get(id_compra=id_compra)
        form = OrdenesForm(request.POST or None, instance=instance)
        detalle = DetalleOrdenCompra.objects.filter(id_compra=id_compra)
        aux_prod = 0
        aux_color = ""
        dic_detalle = {}
        talla=[]
        cantidad=[]
        i=-1
        for d in detalle:
                if aux_prod == d.id_producto.id_producto and aux_color == d.color:
                    talla.append(d.talla)
                    cantidad.append(d.cantidad)
                    precio_venta=round((d.precio+(Decimal(d.precio)*Decimal(d.porcentaje_ganancia/100))),2)
                    dic_detalle.update(
                        {i:{
                            'id_producto':d.id_producto.id_producto,
                            'nombre_producto':d.id_producto.descripcion,
                            'precio_producto':str(round(d.precio,2)),
                            'porcentaje_ganancia':str(d.porcentaje_ganancia),
                            'precio_venta':str(precio_venta),
                            'nombre_color':d.nombre_color,
                            'color':d.color,
                            'detalle':{'talla':talla,'cantidad':cantidad}
                            }
                        })
                else:
                    i=i+1
                    talla=[]
                    cantidad=[]
                    aux_prod=d.id_producto.id_producto
                    aux_ =d.color
                    precio_venta=round((d.precio+(Decimal(d.precio)*Decimal(d.porcentaje_ganancia/100))),2)
                    talla.append(d.talla)
                    cantidad.append(d.cantidad)
                    dic_detalle.update(
                        {i:{
                            'id_producto':d.id_producto.id_producto,
                            'nombre_producto':d.id_producto.descripcion,
                            'precio_producto':str(round(d.precio,2)),
                            'porcentaje_ganancia':str(d.porcentaje_ganancia),
                            'precio_venta':str(precio_venta),
                            'nombre_color':d.nombre_color,
                            'color':d.color,
                            'detalle':{'talla':talla,'cantidad':cantidad}
                            }
                        })
    
        data=json.dumps(dic_detalle)
        return render(request, 'ordenes_modificar.html', {'form': form, 'detalle':data,'orden':id_compra})
    return render(request, 'ordenes_modificar.html')


def Orden_Previsualizar(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    proveedor = request.POST['proveedor']
    prov = Proveedores.objects.filter(id_proveedor=proveedor)

    prov_fields = (
        'nombre_empresa',
        'direccion_fiscal',
        'dni_rif_documento',
        'telefono',
    )

    data = serializers.serialize('json', prov, fields=prov_fields)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def enviar_correo(request):
    id=str(request.POST['id'])
    mail = EmailMessage(
        'Orden de Compra',
        'Vima genero una orden de compra para usted, detalles en el adjunto.',
        to=[request.POST['correo']],
    )
    fp = open(os.path.join(settings.MEDIA_ROOT+'\\Ordenes\\orden_compra_'+id+'.pdf'), 'rb')
    adjunto = MIMEBase('multipart', 'encrypted')
    adjunto.set_payload(fp.read())
    fp.close()
    encoders.encode_base64(adjunto)
    adjunto.add_header('Content-Disposition', 'attachment',
                       filename='orden_compra.pdf')
    mail.attach(adjunto)
    mail.send()

    return HttpResponse({'mensaje' : 'Orden Procesada satisfactoriamente. El proveedor recibirá ésta orden por correo electrónico.'}, content_type="application/json")

@csrf_exempt
def Recepcion_Crear(request):
    if request.is_ajax():
        diferencia=0
        orden = OrdenesCompras.objects.get(
            id_compra=request.POST['orden'])
        usuario = User.objects.get(id=int(request.POST['usuario']))
        # Crear la Recepcion
        try:
            with transaction.atomic():
                recepcion = RecepcionCompras(id_orden_compra=orden, id_usuario=usuario, observaciones=request.POST['observacion'], fecha_recepcion=request.POST['fecha'], estado=request.POST['estado'],monto=request.POST['monto'],porcentaje_impuesto=request.POST['porc_impuesto'],impuesto=request.POST['impuesto'],porcentaje_flete=request.POST['porc_flete'],monto_flete=request.POST['flete'])
                recepcion.save()
                # Crear el Detalle de la Recepcion
                for indx in range(int(request.POST['cantidad_detalle'])):
                    id_detalle=producto = request.POST['detalle[' + str(indx) + '][pk]']
                    producto = 'detalle[' + str(indx) + '][codigo_producto]'
                    lote = request.POST['detalle[' + str(indx) + '][lote]']
                    talla = request.POST['detalle[' + str(indx) + '][talla]']
                    ncol = request.POST['detalle[' + str(indx) + '][nombre_color]']
                    color = request.POST['detalle[' + str(indx) + '][color]']
                    req = request.POST['detalle[' + str(indx) + '][requerida]']
                    rec = request.POST['detalle[' + str(indx) + '][recibida]']
                    prod = Productos.objects.get(
                        id_producto=int(request.POST[producto]))

                    detalle_orden=DetalleOrdenCompra.objects.get(id_detalle=id_detalle)
                    

                    detalle = DetalleRecepcionCompra(id_recepcion=recepcion, id_producto=prod, id_lote=lote, talla=talla, color=color, nombre_color=ncol, cantidad=int(
                        req), cantidad_recibida=int(rec), precio=Decimal(detalle_orden.precio), total=int(rec) * Decimal(detalle_orden.precio))
                    detalle.save()
                    # Actualizar cantidad recibida en orden de compra
                    detalle_orden.cantidad_recibida=int(detalle_orden.cantidad_recibida)+int(rec)
                    detalle_orden.save()
                    if int(req)>int(rec):
                        diferencia=diferencia+1

                    # Actualizar existencias en almacen
                    existencia = ProductosAlmacen.objects.filter(id_producto=prod.id_producto,talla=talla,color=color,id_almacen=orden.id_almacen)
                    if existencia.exists():
                        existencia=existencia[0]
                        existencia.existencia=existencia.existencia+int(rec)
                        existencia.precio_venta=detalle_orden.precio+(detalle_orden.precio*Decimal(detalle_orden.porcentaje_ganancia/100))
                        existencia.save()
                    else:
                        existencia=ProductosAlmacen(id_producto=prod,talla=talla,color=color,nombre_color=ncol,id_almacen=orden.id_almacen,publicado=0,precio_venta=detalle_orden.precio+(detalle_orden.precio*Decimal(detalle_orden.porcentaje_ganancia/100)),existencia=int(rec))
                        existencia.save()
                # Cambiar estado de la orden
                if diferencia>0:
                    orden.estado="Parcial"
                    orden.save()
                else:
                    orden.estado="Procesada"
                    orden.save()
        except IntegrityError:
            form.addError(usuario + ' No se pudo guardar la recepcion.')


        return HttpResponse(json.dumps({'mensaje': 'bien','recepcion':recepcion.id_recepcion, 'estado':orden.estado}), content_type="application/json")

    return render(request, 'recepcion_crear.html', {'form': RecepcionForm})


class Recepcion_Listado(ListView):
    model = RecepcionCompras
    template_name = 'recepcion_listar.html'
    paginate_by = 20

    def get_queryset(self):
        return RecepcionCompras.objects.all().order_by('-id_recepcion','-estado')


def Recepcion_Anular(request, id_recepcion):
    recepcion = RecepcionCompras.objects.get(id_recepcion=id_recepcion)

    if request.method == 'POST':
        detalle_recepcion=DetalleRecepcionCompra.objects.filter(id_recepcion=recepcion.id_recepcion)
        for d in detalle_recepcion:
            # Reducir la cantidad de recibidos en la orden
            detalle_orden=DetalleOrdenCompra.objects.get(id_compra=recepcion.id_orden_compra.id_compra,id_producto=d.id_producto.id_producto,talla=d.talla,color=d.color)
            detalle_orden.cantidad_recibida=detalle_orden.cantidad_recibida-d.cantidad_recibida
            detalle_orden.save()
            # Reducir la existencia en almacen
            existencia=ProductosAlmacen.objects.get(id_almacen=recepcion.id_orden_compra.id_almacen,id_producto=d.id_producto.id_producto,talla=d.talla,color=d.color)
            existencia.existencia=existencia.existencia-d.cantidad_recibida
            existencia.save()
        #anular cuenta por pagar
        if recepcion.estado=="Facturada":
            cuenta= CuentasPagar.objects.get(id_recepcion=recepcion.id_recepcion)
            cuenta.estado="Anulada"
            cuenta.save()
        #cambiar estado de la orden
        orden=OrdenesCompras.objects.get(id_compra=recepcion.id_orden_compra.id_compra)
        orden.estado="Parcial"
        orden.save()
        #cambiar estado de la recepcion
        recepcion.estado = 'Anulada'
        recepcion.save()
        
        return redirect('compras:recepcion_listado')

    return render(request, 'recepcion_anular.html', {'recepcion': recepcion})


def Recepcion_Detalle(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    orden = request.POST['orden']
    detalle = DetalleOrdenCompra.objects.filter(id_compra=orden)

    dic_detalle = {}
    i=0
    for d in detalle:
        if d.cantidad-d.cantidad_recibida:
            precio_venta=round((d.precio+(Decimal(d.precio)*Decimal(d.porcentaje_ganancia/100))),2)
            dic_detalle.update(
                {i:{
                    'pk':d.pk,
                    'id_producto':d.id_producto.id_producto,
                    'nombre_producto':d.id_producto.descripcion,
                    'precio':str(round(d.id_producto.precio_proveedor,2)),
                    'precio_venta':str(precio_venta),
                    'nombre_color':d.nombre_color,
                    'color':d.color,
                    'talla':d.talla,
                    'cantidad':d.cantidad-d.cantidad_recibida,
                    'cantidad_recibida':d.cantidad_recibida
                    }
                })
            i=i+1

    data = json.dumps(dic_detalle)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def Recepcion_Mostrar(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    id = request.POST['recepcion']
    recepcion = RecepcionCompras.objects.filter(id_recepcion=id)
    detalle= DetalleRecepcionCompra.objects.filter(id_recepcion=id)

    rec_fields = (
        'id_orden_compra',
        'fecha_recepcion',
        'observaciones',
        'monto',
        'impuesto',
        'porcentaje_impuesto',
        'monto_flete',
        'porcentaje_flete',
    )

    dic_detalle = {}
    i=0
    for d in detalle:
        dic_detalle.update(
            {i:{
                'id_producto':d.id_producto.id_producto,
                'nombre_producto':d.id_producto.descripcion,
                'precio':str(round(d.id_producto.precio_proveedor,2)),
                'nombre_color':d.nombre_color,
                'talla':d.talla,
                'cantidad_recibida':d.cantidad_recibida,
                'total':str(d.total),
                }
            })
        i=i+1

    data2 = json.dumps(dic_detalle)
    data = serializers.serialize('json', recepcion, fields=rec_fields)
    resp= json.dumps({'recepcion':data,'detalle':data2})
    return HttpResponse(resp, content_type="application/json")



class DescargarReporte(JasperReportDescargar):

    report_name = 'orden'

    def __init__(self, orden):
        self.orden = orden
        self.filename = 'orden_compra_{}'.format(self.orden.id_compra)
        super(DescargarReporte, self).__init__()

    def get_params(self):
        return {
            'orden': self.orden.id_compra
        }


class MostrarReporte(JasperReportMostrar):

    report_name = 'orden'

    def __init__(self, orden):
        self.orden = orden
        self.filename = 'orden_compra_{}'.format(self.orden.id_compra)
        super(MostrarReporte, self).__init__()

    def get_params(self):
        return {
            'orden': self.orden.id_compra
        }


def descargar_pdf(request,id_compra):  
    orden=OrdenesCompras.objects.get(id_compra=id_compra)
    report = DescargarReporte(orden)

    return report.render_to_response()


def mostrar_pdf(request,id_compra):  
    orden=OrdenesCompras.objects.get(id_compra=id_compra)
    report = MostrarReporte(orden)
    return report.render_to_response()
