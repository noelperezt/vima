from django.apps import AppConfig


class CuentasPagarConfig(AppConfig):
    name = 'cuentas_pagar'
