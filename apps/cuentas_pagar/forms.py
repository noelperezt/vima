from django import forms
from django.core.files.images import get_image_dimensions
from django.utils.translation import ugettext_lazy as _
from apps.cuentas_pagar.models import CuentasPagar, Pagos, Proveedores


class ProveedorForm(forms.ModelForm):
    """Definición del formulario para guardar un proveedor"""

    class Meta:
        model = Proveedores
        fields = [
            'nombre_empresa',
            'descripcion',
            'dni_rif_documento',
            'representante_legal',
            'correo',
            'direccion_fiscal',
            'pais_direccion_fiscal',
            'zip_direccion_fiscal',
            'direccion_envio',
            'pais_direccion_envio',
            'zip_direccion_envio',
            'logo',
            'catalogo',
            'es_disenador',
            'nombre_disenador',
            'activo',
        ]
        widgets = {
            'nombre_empresa':
            forms.TextInput(attrs={
                'class': 'form-control',
                'nombre_etiqueta': 'El nombre de la empresa',
                'required': 'required'
            }),
            'descripcion':
            forms.Textarea(attrs={
                'class': 'form-control',
                'cols': '20',
                'rows': '5',
                'style': 'resize: none;',
                'nombre_etiqueta': 'La descripción de la empresa',
                'required': 'required'
            }),
            'dni_rif_documento':
            forms.TextInput(attrs={
                'class': 'form-control',
                'onKeyPress': 'return soloNumeros(event);',
                'nombre_etiqueta': 'El documento de identidad',
                'required': 'required'
            }),
            'representante_legal':
            forms.TextInput(attrs={
                'class': 'form-control',
                'nombre_etiqueta': 'El representante',
                'required': 'required'
            }),
            'correo':
            forms.EmailInput(attrs={
                'class': 'form-control',
                'nombre_etiqueta': 'El correo',
                'required': 'required'
            }),
            'direccion_fiscal':
            forms.Textarea(attrs={
                'class': 'form-control',
                'cols': '20',
                'rows': '5',
                'style': 'resize: none;',
                'nombre_etiqueta': 'La dirección fiscal',
                'required': 'required'
            }),
            'pais_direccion_fiscal':
            forms.Select(attrs={
                'class': 'form-control',
                'nombre_etiqueta': 'El país',
                'required': 'required'
            }),
            'zip_direccion_fiscal':
            forms.TextInput(attrs={
                'class': 'form-control',
                'onKeyPress': 'return soloNumeros(event);',
                'nombre_etiqueta': 'El código postal',
                'required': 'required'
            }),
            'direccion_envio':
            forms.Textarea(attrs={
                'class': 'form-control',
                'cols': '20',
                'rows': '5',
                'style': 'resize: none;',
                'nombre_etiqueta': 'La dirección de envío',
                'required': 'required'
            }),
            'pais_direccion_envio':
            forms.Select(attrs={
                'class': 'form-control',
                'nombre_etiqueta': 'El país',
                'required': 'required'
            }),
            'zip_direccion_envio':
            forms.TextInput(attrs={
                'class': 'form-control',
                'onKeyPress': 'return soloNumeros(event);',
                'nombre_etiqueta': 'El codigo postal',
                'required': 'required'
            }),
            'logo':
            forms.FileInput(attrs={
                'class': 'file-upload',
                'accept': 'image/*',
                'onchange': 'readURL(this);',
                'nombre_etiqueta': 'El logo',
                'required': 'required'
            }),
            'catalogo':
            forms.FileInput(attrs={
                'class': 'file-upload',
                'accept': 'application/pdf',
                'onchange': 'readURL(this);',
                'nombre_etiqueta': 'El catálogo',
                'required': 'required'
            }),
            'es_disenador':
            forms.RadioSelect(choices=[(True, _('Si')), (False, _('No'))]),
            'nombre_disenador':
            forms.TextInput(attrs={
                'class': 'form-control',
                'nombre_etiqueta': 'El nombre de diseñador',
                'style':'display:none;'
            }),
            'activo':
            forms.CheckboxInput(attrs={
                'class': 'checkbox-inline',
                'nombre_etiqueta': 'activo',
                'required': 'required'
            })
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for campo in self.fields:
            if campo == 'activo':
                self.fields[campo].help_text = None

    def clean_logo(self):
        """validación de tamaño de imagen"""
        picture = self.cleaned_data.get('logo')
        if not picture:
            raise forms.ValidationError(_("Debe seleccionar una imagen!"))
        else:
            image_width, dummy = get_image_dimensions(picture)
            if image_width < 380:
                raise forms.ValidationError(
                    _("La imagen debe tener al menos 380 píxeles de ancho"))
        return picture


class CuentasPagarFormCrear(forms.ModelForm):
    class Meta:
        model = CuentasPagar
        fields = [
            'id_proveedor',
            'concepto',
            'tipo_transaccion',
            'numero_documento',
            'numero_control',
            'fecha_documento',
            'fecha_vencimiento',
            'subtotal',
            'monto',
            'impuesto',
            'porcentaje_impuesto',
            'monto_flete',
            'porcentaje_flete',
            'id_usuario',
            'estado',
            'id_recepcion',
        ]
        widgets = {
            'concepto':
            forms.TextInput(attrs={'style': 'width: 100%'}),
            'numero_documento':
            forms.TextInput(attrs={
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);'
            }),
            'numero_control':
            forms.TextInput(attrs={
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);'
            }),
            'fecha_documento':
            forms.DateInput(attrs={
                'class': 'datepicker',
                'date-date-format': 'YYYY-MM-DD',
                'style': '    width: 100%',
                'required': True
            }),
            'fecha_vencimiento':
            forms.DateInput(attrs={
                'class': 'datepicker',
                'date-date-format': 'YYYY-MM-DD',
                'style': '    width: 100%',
                'required': True
            }),
            'subtotal':
            forms.TextInput(attrs={
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);',
                'onfocusout': 'calculoTotal();'
            }),
            'monto':
            forms.TextInput(attrs={
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);'
            }),
            'porcentaje_impuesto':
            forms.NumberInput(attrs={
                'minlength': 1,
                'maxlength': 100,
                'required': True,
                'type': 'number',
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);',
                'onfocusout': 'calculoImpuesto();'
            }),
            'impuesto':
            forms.TextInput(attrs={
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);',
                'onfocusout': 'calculoTotal();'
            }),
            'porcentaje_flete':
            forms.NumberInput(attrs={
                'minlength': 1,
                'maxlength': 100,
                'required': True,
                'type': 'number',
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);',
                'onfocusout': 'calculoflete();'
            }),
            'monto_flete':
            forms.TextInput(attrs={
                'style': '    width: 100%',
                'onKeyPress': 'return soloNumeros(event);',
                'onfocusout': 'calculoTotal();'
            }),
            'id_proveedor':
            forms.HiddenInput(),
            'tipo_transaccion':
            forms.HiddenInput(),
            'id_usuario':
            forms.HiddenInput(),
            'estado':
            forms.HiddenInput(),
            'id_recepcion':
            forms.HiddenInput(),
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for campo in self.fields:
                if campo == 'activo':
                    self.fields[campo].help_text = None


class PagosForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.id_cuenta_pagar = kwargs.pop('id_cuenta', None)
        super(PagosForm, self).__init__(*args, **kwargs)
        self.fields['tipo'].error_messages.update({
            'required':
            _('Debe seleccionar el tipo de pago')
        })
        self.fields['monto'].error_messages.update({
            'required':
            _('Debe indicar el monto del pago')
        })
        self.fields['referencia'].error_messages.update({
            'required':
            _('Debe indicar la referencia del pago')
        })

    def clean(self):
        cuenta = CuentasPagar.objects.get(id_cuenta_pagar=self.id_cuenta_pagar)
        monto_usuario = self.cleaned_data.get('monto')
        if monto_usuario is not None:
            if monto_usuario == 0:
                raise forms.ValidationError(
                    _('El monto del pago o abono no puede ser cero'))
            if cuenta.saldo < monto_usuario:
                raise forms.ValidationError(
                    _('No se puede cancelar un monto mayor al saldo'))
        return self.cleaned_data

    class Meta:
        model = Pagos
        fields = [
            'concepto',
            'tipo',
            'monto',
            'referencia',
            'id_cuenta_pagar',
        ]
        widgets = {
            'tipo':
            forms.Select(attrs={'class': 'form-control'}),
            'concepto':
            forms.TextInput(attrs={'class': 'form-control'}),
            'monto':
            forms.TextInput(attrs={
                'class': 'form-control',
                'onKeyPress': 'return soloNumeros(event);'
            }),
            'referencia':
            forms.TextInput(attrs={'class': 'form-control'}),
            'id_cuenta_pagar':
            forms.HiddenInput()
        }
