""" Modelos de la app cuentas_pagar """
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core import validators
from django.conf import settings
from django.contrib.auth.models import User


class Paises(models.Model):
    """
        Modelo para lista de países.
    """
    id_pais = models.CharField(
        primary_key=True,
        max_length=3,
        blank=False,
        null=False,
        verbose_name=_('código del país'))
    nombre_pais = models.CharField(
        max_length=250,
        blank=False,
        null=False,
        verbose_name=_('nombre del país'))

    class Meta:
        db_table = 'paises'
        verbose_name = _('Pais')
        verbose_name_plural = _('Paises')

    def __str__(self):
        return self.nombre_pais


class Ciudades(models.Model):
    """
        Modelo para lista de ciudades.
    """
    id_ciudad = models.AutoField(
        primary_key=True,
        blank=False,
        null=False,
        verbose_name=_('código del país'))
    nombre_ciudad = models.CharField(
        max_length=250,
        blank=False,
        null=False,
        verbose_name=_('nombre del país'))
    id_pais = models.ForeignKey(
        Paises,
        on_delete=models.DO_NOTHING,
        blank=False,
        null=False,
        verbose_name=_('país donde se encuentra la ciudad'))

    class Meta:
        db_table = 'ciudades'
        verbose_name = _('ciudad')
        verbose_name_plural = _('ciudades')

    def __str__(self):
        return self.nombre_ciudad


class Proveedores(models.Model):
    """
        Definición de Proveedor.
    """
    id_proveedor = models.AutoField(primary_key=True)
    id_usuario = models.OneToOneField(
        User, on_delete=models.CASCADE, null=True)
    nombre_empresa = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('Empresa'))
    descripcion = models.TextField(
        null=False,
        blank=False,
        verbose_name=_('descripción de la empresa'),
        validators=[
            validators.MinLengthValidator(
                300, message=_('Debe escribir al menos 300 caracteres'))
        ])
    direccion_fiscal = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('dirección Fiscal'))
    pais_direccion_fiscal = models.ForeignKey(
        Paises,
        related_name='pais_direccion_fiscal',
        null=False,
        blank=False,
        default='',
        verbose_name=_('país'))
    ciudad_direccion_fiscal = models.ForeignKey(
        Ciudades,
        related_name='ciudad_direccion_fiscal',
        null=True,
        blank=True,
        default=0,
        verbose_name=_('ciudad'))
    zip_direccion_fiscal = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('codigo postal'))
    direccion_envio = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('dirección para envíos de la empresa'))
    pais_direccion_envio = models.ForeignKey(
        Paises,
        related_name='pais_direccion_envio',
        null=False,
        blank=False,
        default='',
        verbose_name=_('pais'))
    ciudad_direccion_envio = models.ForeignKey(
        Ciudades,
        related_name='ciudad_direccion_envio',
        null=True,
        blank=True,
        default=0,
        verbose_name=_('ciudad'))
    zip_direccion_envio = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('codigo postal'))
    dni_rif_documento = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        db_index=True,
        verbose_name=_('documento de Identificación (DNI)'))
    representante_legal = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('representante'))
    codigo_postal = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        default='',
        verbose_name=_('código de área'))
    logo = models.FileField(
        upload_to='logo_proveedores/',
        null=True,
        verbose_name=_('logo del proveedor'))
    catalogo = models.FileField(
        upload_to='catalogo_proveedores/',
        null=True,
        verbose_name=_('catalogo del proveedor'))
    telefono = models.TextField(
        max_length=200, null=False, blank=False, verbose_name=_('teléfonos'))
    correo = models.EmailField(
        max_length=100,
        null=False,
        blank=False,
        verbose_name=_('correo electrónico'),
        help_text=
        _('Requerido. 30 caracteres o menos. Solamente letras, dígitos y @/./+/-/_.'
          ),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                _('Ingrese un correo válido. Sólo puede contener letras, números y @/./+/-/_.'
                  )),
        ])
    fecha_registro = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_('fecha de registro'))
    es_disenador = models.BooleanField(
        blank=False,
        default=False,
        verbose_name=_('Indica si el proveedor es diseñador'))
    nombre_disenador = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        default='',
        verbose_name=
        _('descripción alternativa si el nombre de proveedor es distinto de la marca'
          ))
    activo = models.BooleanField(
        blank=False, default=True, verbose_name=_('activo'))

    class Meta:
        db_table = 'proveedores'
        verbose_name = 'proveedor'
        verbose_name_plural = 'proveedores'

    def __str__(self):
        return self.nombre_empresa


class CuentasPagar(models.Model):
    """Definición de Cuentas por pagar."""

    id_cuenta_pagar = models.AutoField(primary_key=True)
    id_proveedor = models.ForeignKey(
        Proveedores,
        null=False,
        blank=False,
        default=0,
        verbose_name=_('Proveedor Relacionado'))
    id_recepcion = models.IntegerField(
        null=False,
        blank=False,
        default=0,
        verbose_name=_('Recepcion Relacionada'))
    concepto = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name=_('Descripción del Compromiso de Pago'))
    tipo_transaccion = models.CharField(
        max_length=15,
        null=False,
        blank=False,
        verbose_name=_('Tipo de Transacción'))
    numero_documento = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        verbose_name=_('Número del Compromiso de Pago'))
    numero_control = models.CharField(
        max_length=100,
        null=False,
        blank=False,
        default=0,
        verbose_name=_('Número de control de la Factura'))
    fecha_documento = models.DateField(
        editable=True, verbose_name=_('Fecha del Documento'))
    fecha_vencimiento = models.DateField(
        editable=True, verbose_name=_('Fecha de Vencimiento'))
    fecha_registro = models.DateField(
        auto_now_add=True, editable=False, verbose_name=_('Fecha de Registro'))
    subtotal = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('subtotal de factura'))
    monto = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('Monto del Compromiso'))
    saldo = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('Saldo del Compromiso'))
    impuesto = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('Monto de Impuesto'))
    porcentaje_impuesto = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('Porcentaje de Impuesto'))
    monto_flete = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('Monto por Flete'))
    porcentaje_flete = models.IntegerField(
        default=0.0, verbose_name=_('Porcentaje por Flete'))
    id_usuario = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        verbose_name=_('Usuario que Registra el Compromiso'))
    estado = models.CharField(
        max_length=10,
        null=False,
        blank=False,
        verbose_name=_('Estado del Compromiso'))

    class Meta:
        db_table = 'cuentas_por_pagar'
        verbose_name = 'Cuenta por Pagar'
        verbose_name_plural = 'Cuentas por Pagar'


class TiposPagos(models.Model):
    """ Definicion del tipo de transaccion que puede tener un Pago."""

    id_tipo_pago = models.AutoField(primary_key=True)
    descripcion = models.CharField(
        max_length=60,
        null=False,
        blank=False,
        default='',
        verbose_name=_('Descripcion del tipo de pago'))

    class Meta:
        db_table = 'tipos_pagos'
        verbose_name = 'tipo de pago'
        verbose_name_plural = 'tipos de pagos'

    def __str__(self):
        return self.descripcion


class Pagos(models.Model):
    """ Definición de un pago de factura."""

    id_pago = models.AutoField(primary_key=True)
    id_cuenta_pagar = models.ForeignKey(
        CuentasPagar,
        null=True,
        blank=True,
        verbose_name=_('Cuenta por Pagar que se Cancela'))
    concepto = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='CANCELACION DE FACTURA: ',
        verbose_name=_('Descripción del Pago'))
    tipo = models.ForeignKey(
        TiposPagos, null=False, blank=False, verbose_name=_('Tipo de Pago'))
    monto = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('Monto del Pago'))
    referencia = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_('Referencia del Pago'))

    class Meta:
        db_table = 'pagos'
        verbose_name = 'pago'
        verbose_name_plural = 'pagos'
