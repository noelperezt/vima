from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from apps.cuentas_pagar.views import ProveedorListar, ProveedorCrear, ProveedorModificar, ProveedorEliminar,\
 ProveedoresDebtListar, CuentasPagarCrear, CuentasPagarListar, VistaPagosAjax, mostrar_pdf, mostrar_pdf_prov

urlpatterns = [
    url(r'^proveedor$',login_required(ProveedorListar.as_view()), name='listar_proveedores'),
    url(r'^proveedor/nuevo$', login_required(ProveedorCrear.as_view()), name='crear_proveedores'),
    url(r'^proveedor/editar/(?P<pk>\d+)$',login_required(ProveedorModificar.as_view()), name='editar_proveedores'),
    url(r'^proveedor/borrar/(?P<pk>\d+)$',login_required(ProveedorEliminar.as_view()), name='eliminar_proveedores'),
    url(r'^estado_cuenta/$',login_required(ProveedoresDebtListar.as_view()), name='listar_cuentas_pagar'),    
    url(r'^estado_cuenta/get_debt_proveedor/(?P<pk>\d+)$$',login_required(CuentasPagarListar.as_view()), name='buscar_deuda_proveedor'),      
    url(r'^estado_cuenta/settle_debt_proveedor/(?P<id_cuenta_pagar>\d+)$',login_required(VistaPagosAjax.as_view()), name='cancelar_deuda_proveedor'),
    url(r'^facturar/(?P<id_recepcion>\d+)/$', 
        login_required(CuentasPagarCrear.as_view()), name='recepcion_facturar'),
    url(r'^proveedor/listar/(?P<estado>\d+)/$', login_required(mostrar_pdf),
        name='listado_proveedores'),
    url(r'^estado_cuenta/mostrar/(?P<prov>\d+)/$', login_required(mostrar_pdf_prov),
        name='mostrar_estado_cuenta'),
]