"""
    Vistas de la aplicación cuentas_pagar
"""
import json
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count, Sum, F
from django.shortcuts import redirect, render
from django.views.generic import ListView
from django.views.generic.edit import (CreateView, DeleteView, UpdateView)
from django.http import (HttpResponseRedirect, JsonResponse)

from apps.compras.Clases.report import JasperReportMostrar
from apps.compras.models import OrdenesCompras, RecepcionCompras
from apps.cuentas_pagar.forms import (CuentasPagarFormCrear,
                                      PagosForm, ProveedorForm)
from django.contrib.auth.models import User
from apps.cuentas_pagar.models import CuentasPagar, Proveedores, Ciudades
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import ugettext_lazy as _

class ProveedorListar(ListView):
    model = Proveedores
    queryset = Proveedores.objects.all().order_by('id_proveedor')
    template_name = 'proveedores_listar.html'
    context_object_name = 'proveedores'
    paginate_by = 10


class ProveedorCrear(SuccessMessageMixin,CreateView):
    model = Proveedores
    template_name = 'proveedores_crear.html'
    success_url = reverse_lazy('cuentas_pagar:listar_proveedores')
    form_class = ProveedorForm
    success_message = _('Proveedor %(nombre_empresa)s creado correctamente')

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            proveedor = form.save(commit=False)
            usuario = User()
            try:
                usuario.first_name, usuario.last_name = proveedor.representante_legal.split(' ')
            except ValueError:
                pass
            usuario.email = proveedor.correo
            usuario.username = request.POST.get("usuario_proveedor")
            usuario.set_password(request.POST.get("password1"))
            usuario.is_staff = False
            usuario.save()
            proveedor.id_usuario = usuario
            proveedor.activo = True
            proveedor.save()
            return HttpResponseRedirect( reverse_lazy('cuentas_pagar:listar_proveedores'))
        else:
            return  self.form_invalid(form)

class ProveedorModificar(UpdateView):
    model = Proveedores
    template_name = 'proveedores_modificar.html'
    success_url = reverse_lazy('cuentas_pagar:listar_proveedores')
    form_class = ProveedorForm
    success_message = _('Proveedor %(nombre_empresa)s actualizado correctamente')


class ProveedorEliminar(DeleteView):
    model = Proveedores
    template_name = 'proveedores_eliminar.html'
    success_url = reverse_lazy('cuentas_pagar:listar_proveedores')


class ProveedoresDebtListar(ListView):
    model = CuentasPagar
    queryset = CuentasPagar.objects.values(
            'id_proveedor_id', 'id_proveedor_id__descripcion').annotate(
                total_saldo=Sum('saldo'),
                total_monto=Sum('monto'),
                facturas=Count('*')).order_by('id_proveedor_id')
    template_name = 'cuentas_pagar_listar.html'
    context_object_name = 'listado_deuda'
    paginate_by = 10


class CuentasPagarListar(ListView):
    model = CuentasPagar
    template_name = 'cuentas_pagar_facturas.html'
    context_object_name = 'listado_facturas'
    paginate_by = 10

    def get_queryset(self):
        return CuentasPagar.objects.filter(
            id_proveedor=self.kwargs['pk']).annotate(
                abono=F('monto') - F('saldo'))

    def get_context_data(self, **kwargs):
        context = super(CuentasPagarListar, self).get_context_data(**kwargs)
        context['proveedor'] = Proveedores.objects.values('descripcion','id_proveedor').get(
            id_proveedor=self.kwargs['pk'])
        return context


class TemplateNormalAjax(object):
    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'ajax_template_name'):
            split = self.template_name.split('.html')
            split[-1] = '_modal'
            split.append('.html')
            self.ajax_template_name = ''.join(split)
            if request.is_ajax():
                self.template_name = self.ajax_template_name
            return super(TemplateNormalAjax, self).dispatch(
                request, *args, **kwargs)


class VistaPagosAjax(TemplateNormalAjax, CreateView):
    template_name = 'form_pagar.html'
    form_class = PagosForm

    def get_context_data(self, **kwargs):
        context = super(VistaPagosAjax, self).get_context_data(**kwargs)
        context['id_cuenta'] = self.kwargs['id_cuenta_pagar']
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(
            request.POST, id_cuenta=kwargs['id_cuenta_pagar'])
        factura = CuentasPagar.objects.get(
            id_cuenta_pagar=kwargs['id_cuenta_pagar'])
        if form.is_valid():
            pago_cxp = form.save(commit=False)
            pago_cxp.id_cuenta_pagar = factura
            pago_cxp.save()
            factura.saldo = factura.saldo - pago_cxp.monto
            factura.save()
            return JsonResponse({
                'mensaje': 'exito',
                'id_proveedor': factura.id_proveedor_id
            })
        else:
            return self.form_invalid(form)


class CuentasPagarCrear(CreateView):
    model = CuentasPagar
    template_name = 'recepcion_facturar.html'
    success_url = reverse_lazy('compras:recepcion_listado')
    form_class = CuentasPagarFormCrear 

    def get_context_data(self, **kwargs):
        context = super(CuentasPagarCrear, self).get_context_data(**kwargs)
        recepcion=RecepcionCompras.objects.get(id_recepcion=self.kwargs.get('id_recepcion', 0))
        context['recepcion'] = recepcion
        monto=RecepcionCompras.objects.filter(id_orden_compra=recepcion.id_orden_compra.id_compra).aggregate(monto=Sum('monto'))
        context['sub'] =round(monto['monto'],2)
        return context

    def post(self, request, *args, **kwargs):
        form = CuentasPagarFormCrear(request.POST)
        if form.is_valid():
            cuenta_pagar = form.save(commit=False)
            cuenta_pagar.saldo = cuenta_pagar.monto
            cuenta_pagar.save()
            recepcion = RecepcionCompras.objects.get(id_recepcion=self.kwargs['id_recepcion'])
            recepcion.estado = 'Facturada'
            recepcion.save()
            orden = OrdenesCompras.objects.get(
                id_compra=recepcion.id_orden_compra.id_compra)
            orden.estado = "Procesada"
            orden.save()
            return HttpResponseRedirect(reverse_lazy('compras:recepcion_listado'))


class MostrarReporte(JasperReportMostrar):

    report_name = 'proveedores'

    def __init__(self, estado):
        self.estado = estado
        self.filename = 'proveedores_{}'.format(self.estado)
        super(MostrarReporte, self).__init__()

    def get_params(self):
        return {'estado': self.estado}


def mostrar_pdf(request, estado):
    report = MostrarReporte(estado)
    return report.render_to_response()


class MostrarReporteProveedores(JasperReportMostrar):

    report_name = 'estado_cuenta'

    def __init__(self, prov):
        self.prov = prov
        self.filename = 'estado_cuenta_prov_{}'.format(self.prov)
        super(MostrarReporteProveedores, self).__init__()

    def get_params(self):
        return {'prov': self.prov}


def mostrar_pdf_prov(request, prov):
    report = MostrarReporteProveedores(prov)
    return report.render_to_response()


def get_cities_by_countryid(request):
    """Obtiene una lista de ciudades dado un país"""
    # si no es una peticion ajax, devolvemos error 400
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest(request)

    return JsonResponse(
        json.dumps([
            dict(item)
            for item in Ciudades.objects.filter(id_pais=request.POST["pais"])
            .values('id_ciudad','nombre_ciudad')
        ]),
        safe=False)