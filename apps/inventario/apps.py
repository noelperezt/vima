"""
    Declaración de aplicación Inventario.
"""
from django.apps import AppConfig


class InventarioConfig(AppConfig):
    """
        Agrega la aplicación Inventario
    """
    name = 'inventario'
