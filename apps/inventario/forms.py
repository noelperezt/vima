"""
    Formularios de la aplicación inventario.
"""

from django import forms
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _

from apps.inventario.models import (Almacenes, Categorias, Marcas, Productos,
                                    Publicaciones, Responsables, Tallas)
from apps.website.models import DespachoMercancia, Ventas
from apps.cuentas_pagar.models import Proveedores


class AlmacenForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Almacen``
    """

    class Meta:
        model = Almacenes
        fields = ['descripcion', 'direccion', 'activo']
        widgets = {
            'descripcion':
            forms.TextInput(attrs={'class': 'form-control'}),
            'direccion':
            forms.Textarea(
                attrs={'class': 'form-control',
                       'rows': '5',
                       'cols': '20'}),
            'activo':
            forms.CheckboxInput(attrs={'class': 'checkbox-inline'})
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for campo in self.fields:
                if campo == 'activo':
                    self.fields[campo].help_text = None


class ResponsableForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Responsable``
    """

    class Meta:
        model = Responsables
        fields = ['nombre', 'id_usuario']
        labels = {
            'nombre': _('Nombre y apellido'),
            'id_usuario': _('Usuario en el sistema')
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'id_usuario': forms.Select(attrs={'class': 'form-control'})
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for campo in self.fields:
                self.fields[campo].help_text = None


class DepartamentosForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Departamentos``
    """

    class Meta:
        model = Categorias
        fields = ['descripcion']


class CategoriaForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Categoria``
    """

    class Meta:
        model = Categorias
        fields = ['descripcion', 'id_categoria_padre']
        widgets = {
            'descripcion': forms.HiddenInput(),
            'id_categoria_padre': forms.HiddenInput()
        }


class MarcaForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Marca``
    """

    class Meta:
        model = Marcas
        fields = ['descripcion', 'activo']
        widgets = {
            'descripcion':
            forms.TextInput(
                attrs={'class': 'form-control',
                       'required': 'required'}),
            'activo':
            forms.CheckboxInput(attrs={'class': 'checkbox-inline'})
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for campo in self.fields:
                self.fields[campo].help_text = None


def get_grupo_tallas():
    """Obtiene los grupos de tallas"""
    return [
        (i, i)
        for i in Tallas.objects.values_list('id_grupo', flat=True).annotate(
            Count('id_grupo', distinct=True))
    ]


class TallaForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Talla``
    """

    class Meta:
        model = Tallas
        fields = ['descripcion', 'id_grupo']
        widgets = {
            'descripcion':
            forms.TextInput(attrs={
                'class': 'form-control',
                'required': 'required',
                'style': 'text-transform:uppercase'
            }),
            'id_grupo':
            forms.TextInput(attrs={
                'class': 'form-control',
                'required': 'required',
                'style': 'text-transform:uppercase'
            }),
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for campo in self.fields:
                self.fields[campo].help_text = None


class ProductoForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Producto``
    """
    id_proveedor = forms.ModelChoiceField(
        queryset=Proveedores.objects.filter(activo=True),
        widget=forms.Select(attrs={
            'class': 'selectpicker form-control',
            'data-live-search': 'true',
            'rules': 'rules'
        }),
        label="Proveedor")
    id_marca = forms.ModelChoiceField(
        queryset=Marcas.objects.filter(activo=True),
        widget=forms.Select(attrs={
            'class': 'selectpicker form-control',
            'data-live-search': 'true',
            'rules': 'rules'
        }),
        label="Marca")

    grupo_talla = forms.ChoiceField(
        choices=get_grupo_tallas,
        required=False,
        widget=forms.Select(attrs={
            'class': 'form-control',
            'style': 'float:left;',
            'data-width': '130px',
            'title': 'Grupo',
            'required': 'False'
        }))

    class Meta:
        model = Productos
        fields = [
            'descripcion',
            'id_marca',
            'precio_proveedor',
            'id_proveedor',
        ]
        widgets = {
            'descripcion':
            forms.TextInput(attrs={'class': 'form-control'}),
            'precio_proveedor':
            forms.TextInput(attrs={
                'class': 'form-control',
                'onKeyPress': 'return soloNumeros(event);'
            }),
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for campo in self.fields:
                self.fields[campo].help_text = None


class DespachoForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Despacho``
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['id_venta'].widget.attrs.update({
            'class':
            'selectpicker',
            'data-live-search':
            'true',
            'data-width':
            '100%',
            'required':
            'required',
            'title':
            'Proveedores',
        })

    id_venta = forms.ModelChoiceField(queryset=Ventas.objects.filter(
        estado='Sin Enviar'))

    class Meta:
        model = DespachoMercancia
        fields = [
            'id_venta',
            'nro_tracking',
            'courier',
        ]
        widgets = {
            'nro_tracking':
            forms.TextInput(attrs={
                'style': 'width: 100%',
                'onKeyPress': 'return soloNumeros(event);',
                'required': True
            }),
            'courier':
            forms.TextInput(attrs={'class': 'form-control',
                                   'required': True}),
        }


class PublicacionForm(forms.ModelForm):
    """
        Formulario para manejo del modelo ``Publicacion``
    """

    class Meta:
        model = Publicaciones
        fields = ['titulo', 'descripcion']
        widgets = {
            'titulo':
            forms.TextInput(
                attrs={'class': 'form-control',
                       'required': 'required'}),
            'descripcion':
            forms.Textarea(attrs={
                'class': 'form-control',
                'required': 'required',
                'cols': '7',
                'rows': '7'
            }),
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for campo in self.fields:
                self.fields[campo].help_text = None
