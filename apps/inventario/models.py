"""
    Modelos de la aplicación Inventario.
"""
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.cuentas_pagar.models import Proveedores


class Categorias(models.Model):
    """
        Definición de Categorias.
    """
    id_categoria = models.AutoField(primary_key=True)
    descripcion = models.CharField(
        max_length=250, null=False, blank=False, verbose_name=_('descripción'))
    id_categoria_padre = models.ForeignKey(
        'self',
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        default=1,
        verbose_name=_('categoría padre'))

    class Meta:
        db_table = 'categoria_productos'
        verbose_name = 'categoría'
        verbose_name_plural = 'categorías'

    def __str__(self):
        return self.descripcion

    def children(self):
        return Categorias.objects.filter(id_categoria_padre=self.pk)

    def serializable_object(self):
        obj = {'name': self.descripcion, 'hijos': []}
        for hijo in self.children():
            obj['hijos'].append(hijo.serializable_object())
        return obj

    def get_all_children(self, include_self=True):
        """Recursivamente obtiene todos los hijos de una categoria"""
        lista_hijos = []
        if include_self:
            lista_hijos.append(self)
        for categoria in Categorias.objects.filter(id_categoria_padre=self):
            _r = categoria.get_all_children(include_self=True)
            if 0 < len(_r):
                lista_hijos.extend(_r)
        return lista_hijos

    def get_ancestors(self):
        """Recursivamente obtiene todos los padres de una categoria"""
        if self.id_categoria_padre is None:
            return []
        return [self.id_categoria_padre] + self.id_categoria_padre.get_ancestors()


class Marcas(models.Model):
    """
        Definición de Marcas.
    """
    id_marca = models.AutoField(primary_key=True)
    descripcion = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name=_('descripción de la marca'))
    activo = models.BooleanField(
        blank=False, default=True, verbose_name=_('activo'))

    class Meta:
        db_table = 'marcas'
        verbose_name = 'marca'
        verbose_name_plural = 'marcas'

    def __str__(self):
        return self.descripcion


class Productos(models.Model):
    """
        Definición de Productos.
    """
    id_producto = models.AutoField(primary_key=True)
    descripcion = models.CharField(
        max_length=250, null=False, blank=False, verbose_name=_('descripción'))
    id_categoria = models.ForeignKey(
        Categorias,
        null=False,
        blank=False,
        default=0,
        verbose_name=_('categoría'))
    id_proveedor = models.ForeignKey(
        Proveedores,
        null=False,
        blank=False,
        default=0,
        verbose_name=_('proveedor'))
    id_marca = models.ForeignKey(
        Marcas,
        null=False,
        blank=False,
        default=0,
        verbose_name=_('marca del producto'))
    precio_proveedor = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.00,
        verbose_name=_('precio catalogo del proveedor'))
    atributos = models.TextField(
        null=False,
        blank=False,
        default='{}',
        verbose_name=_('atributos del producto'))

    class Meta:
        db_table = 'productos'
        verbose_name = 'producto'
        verbose_name_plural = 'productos'

    def __str__(self):
        return "Objeto: %s %s" % (self.id_producto, self.descripcion)


class Responsables(models.Model):
    """
        Usuario responsable del almacen.
    """
    id_responsable = models.AutoField(primary_key=True)
    nombre = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        default='',
        verbose_name=_('nombre del responsable del almacen'))
    id_usuario = models.ForeignKey(
        User, null=True, blank=True, verbose_name=_('usuario responsable'))

    class Meta:
        db_table = 'responsable_almacen'
        verbose_name = 'responsable'
        verbose_name_plural = 'responsables'


class Almacenes(models.Model):
    """
        Definición de almacenes.
    """
    id_almacen = models.AutoField(primary_key=True)
    descripcion = models.CharField(
        max_length=250, null=False, blank=False, verbose_name=_('descripción'))
    direccion = models.TextField(
        null=True, blank=True, verbose_name=_('dirección del almacen'))
    id_responsable = models.ForeignKey(
        Responsables,
        null=True,
        blank=True,
        verbose_name=_('responsable del almacén'))
    activo = models.BooleanField(
        blank=False, default=True, verbose_name=_('activo'))

    class Meta:
        db_table = 'almacenes'
        verbose_name = 'almacén'
        verbose_name_plural = 'almacenes'

    def __str__(self):
        return self.descripcion


class ProductosAlmacen(models.Model):
    """
        Productos por almacen. Indica la existencia y si el producto está o no publicado.
    """
    id_lote = models.AutoField(primary_key=True)
    id_producto = models.ForeignKey(
        Productos, null=True, blank=True, verbose_name=_('producto'))
    id_almacen = models.ForeignKey(
        Almacenes, null=True, blank=True, verbose_name=_('almacén'))
    talla = models.CharField(
        max_length=20, null=False, blank=True, verbose_name=_('Talla'))
    nombre_color = models.CharField(
        max_length=100,
        null=False,
        blank=True,
        verbose_name=_('nombre del Color'))
    color = models.CharField(
        max_length=10, null=False, blank=True, verbose_name=_('Color'))
    existencia = models.IntegerField(
        default=0, verbose_name=_('cantidad en existencia'))
    contado = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('cantidad contada en inventario'))
    antes = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('cantidad anterior al conteo'))
    despues = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('cantidad despues del conteo'))
    precio_venta = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.0,
        verbose_name=_('precio de venta'))
    publicado = models.BooleanField(default=True, verbose_name=_('publicado'))

    class Meta:
        db_table = 'productos_almacenes'
        verbose_name = 'almacen de productos'


class Tallas(models.Model):
    """
        Definición de Tallas.
    """
    id_talla = models.AutoField(primary_key=True)
    descripcion = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        default='',
        verbose_name=_('descripción de la talla'))
    id_grupo = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        verbose_name=_('grupo de la talla'))

    class Meta:
        db_table = 'talla'
        verbose_name = 'talla'
        verbose_name_plural = 'tallas'

    def __str__(self):
        return self.id_grupo


class Publicaciones(models.Model):
    """Definición del modelo Publicación."""

    id_publicacion = models.AutoField(primary_key=True)
    id_producto = models.ForeignKey(
        Productos, on_delete=models.DO_NOTHING, verbose_name=_('producto'))
    id_lote = models.IntegerField(
        null=False, blank=False, verbose_name=_('id del lote de producto'))
    titulo = models.CharField(
        max_length=250,
        null=False,
        blank=False,
        verbose_name=_('Título de la publicación'))
    colores = ArrayField(models.CharField(max_length=20), blank=True)
    descripcion = models.TextField(
        verbose_name=_('Descripción de la publicación'))
    estado = models.CharField(
        max_length=2,
        null=True,
        blank=False,
        verbose_name=_('estado de la publicación'))

    class Meta:
        db_table = 'publicaciones'
        verbose_name = 'publicaciones'
        verbose_name_plural = 'publicaciones'

    def get_almacen(self):
        """Devuelve el almacen de esta publicación"""
        return ProductosAlmacen.objects.values('id_almacen').get(
            id_lote=self.id_lote)

    def get_precio_venta(self):
        """Devuelve el precio de venta de un producto segun donde esta almacenado"""
        return ProductosAlmacen.objects.values('precio_venta').get(
            id_producto=self.id_producto, id_lote=self.id_lote)

