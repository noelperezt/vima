"""
    Url's de aplicación Inventario.
"""
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from apps.inventario.views import AlmacenListar, AlmacenCrear, AlmacenEliminar,\
AlmacenModificar, CategoriaModificar, CategoriaEliminar, ProductoListar, ProductoCrear,\
ProductoModificar, ProductoEliminar, MarcaCrear, MarcaEliminar, MarcaModificar, \
MarcaListar, Publicar, CategoriasCrear, get_category_children, save_category, \
ExistenciaListar, cargar_almacenes, get_info_producto, get_lote_productos,\
get_talla_productos, PredespachoListar, DespachoListar, DespachoCrear, \
DespachoProcesar, ConteoListar, cargar_categorias, ConteoCargar, ConteoGuardar, \
mostrar_pdf_despacho, mostrar_pdf_porDespachar, mostrar_pdf_conteo, BalanceFiltros, BalanceMostrar, \
Departamento, guardar_deparpamento, buscar_deparpamento, TallaListar, TallaCrear, TallaModificar, TallaEliminar

urlpatterns = [
    #URL's para CRUD de Almacen
    url(r'^almacen$',
        login_required(AlmacenListar.as_view()),
        name='listar_almacenes'),
    url(r'^almacen/nuevo$',
        login_required(AlmacenCrear.as_view()),
        name='crear_almacenes'),
    url(r'^almacen/editar/(?P<pk>\d+)$',
        login_required(AlmacenModificar.as_view()),
        name='editar_almacenes'),
    url(r'^almacen/borrar/(?P<pk>\d+)$',
        login_required(AlmacenEliminar.as_view()),
        name='eliminar_almacenes'),

    #URL's para CRUD de Almacen
    url(r'^talla$',
        login_required(TallaListar.as_view()),
        name='listar_tallas'),
    url(r'^talla/nuevo$',
        login_required(TallaCrear.as_view()),
        name='crear_tallas'),
    url(r'^talla/editar/(?P<pk>\d+)$',
        login_required(TallaModificar.as_view()),
        name='editar_tallas'),
    url(r'^talla/borrar/(?P<pk>\d+)$',
        login_required(TallaEliminar.as_view()),
        name='eliminar_tallas'),

    #URL's para CRUD de Categorias
    url(r'^categoria/nueva$',
        login_required(CategoriasCrear.as_view()),
        name='crear_categorias'),
    url(r'^categoria/editar/(?P<pk>\d+)$',
        login_required(CategoriaModificar.as_view()),
        name='editar_categorias'),
    url(r'^categoria/borrar/(?P<pk>\d+)$',
        login_required(CategoriaEliminar.as_view()),
        name='eliminar_categorias'),
    url(r'^categoria/buscar/$',
        get_category_children,
        name='guardar_categorias'),
    url(r'^categoria/guardar/$', save_category, name='buscar_categorias'),

    #URL's para CRUD de Departamentos
    url(r'^departamentos$',
        login_required(Departamento.as_view()),
        name='listado_departamento'),
    url(r'^departamento/guardar$',
        login_required(guardar_deparpamento),
        name='guardar_departamento'),
    url(r'^departamento/buscar$',
        login_required(buscar_deparpamento),
        name='guardar_departamento'),

    #URL's para CRUD de Productos
    url(r'^producto$',
        login_required(ProductoListar.as_view()),
        name='listar_productos'),
    url(r'^producto/nuevo$',
        login_required(ProductoCrear.as_view()),
        name='crear_productos'),
    url(r'^producto/editar/(?P<pk>\d+)$',
        login_required(ProductoModificar.as_view()),
        name='editar_productos'),
    url(r'^producto/borrar/(?P<pk>\d+)$',
        login_required(ProductoEliminar.as_view()),
        name='eliminar_productos'),
    url(r'^producto/publicar/$',
        login_required(Publicar.as_view()),
        name='publicar_producto'),
    url(r'^producto/informacion/$',
        login_required(get_info_producto),
        name='informacion_producto'),
    url(r'^producto/obtener_lote/$',
        login_required(get_lote_productos),
        name='lote_producto'),
    url(r'^producto/get_talla/$',
        login_required(get_talla_productos),
        name='tallas_producto'),
    #URL's para CRUD de Marcas
    url(r'^marca$',
        login_required(MarcaListar.as_view()),
        name='listar_marcas'),
    url(r'^marca/nuevo$',
        login_required(MarcaCrear.as_view()),
        name='crear_marcas'),
    url(r'^marca/editar/(?P<pk>\d+)$',
        login_required(MarcaModificar.as_view()),
        name='editar_marcas'),
    url(r'^marca/borrar/(?P<pk>\d+)$',
        login_required(MarcaEliminar.as_view()),
        name='eliminar_marcas'),
    #URL's para Existencia en Inventario
    url(r'^existencia$',
        login_required(ExistenciaListar),
        name='listar_existencia'),
    url(r'^cargar$', login_required(cargar_almacenes),
        name='cargar_almacenes'),
    #URL's para Despacho de pedidos
    url(r'^predespacho$',
        login_required(PredespachoListar.as_view()),
        name='predespacho_listar'),
    url(r'^despacho/crear/(?P<id_venta>\d+)$',
        login_required(DespachoCrear),
        name='despacho_crear'),
    url(r'^predespacho/mostrar/(?P<id_despacho>\d+)$',
        login_required(mostrar_pdf_despacho),
        name='predespacho_mostrar'),
    url(r'^despacho$',
        login_required(DespachoListar.as_view()),
        name='despacho_listar'),
    url(r'^despacho/procesar/(?P<id_despacho>\d+)$',
        login_required(DespachoProcesar),
        name='despacho_procesar'),
    url(r'^predespacho/despachar$',
        login_required(mostrar_pdf_porDespachar),
        name='predespacho_despachar'),
    #URL's para conteo de inventario
    url(r'^conteo$', login_required(ConteoListar), name='conteo_listar'),
    url(r'^cargarcat$',
        login_required(cargar_categorias),
        name='cargar_categorias'),
    url(r'^conteo/cargar/(?P<almacen>\d+)/(?P<categoria>\d+)$',
        login_required(ConteoCargar),
        name='conteo_cargar'),
    url(r'^conteo/guardar$',
        login_required(ConteoGuardar),
        name='conteo_guardar'),
    url(r'^conteo/mostrar/(?P<almacen>\d+)/(?P<categoria>\d+)$',
        login_required(mostrar_pdf_conteo),
        name='conteo_mostrar'),
    url(r'^balance/filtros$',
        login_required(BalanceFiltros),
        name='balance_filtrar'),
    url(r'^balance/mostrar/(?P<fecha_inicio>[^/]+)/(?P<fecha_fin>[^/]+)$',
        login_required(BalanceMostrar),
        name='balance_mostrar'),
]
