import json
import os
import uuid

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from itertools import chain
from django.db.models import Aggregate, CharField
from django.db.models.functions import Cast
from django.db.models import Q

from django.conf import settings
from django.contrib import messages
from django.core import serializers
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse_lazy
from django.http import (HttpResponse, HttpResponseBadRequest,
                         HttpResponseRedirect, JsonResponse)
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView
from django.views.generic import CreateView, DeleteView, UpdateView, TemplateView

from apps.inventario.forms import (
    AlmacenForm, CategoriaForm, DespachoForm, MarcaForm, ProductoForm,
    ResponsableForm, PublicacionForm, DepartamentosForm, TallaForm)
from apps.inventario.models import (Almacenes, Categorias, Marcas, Productos,
                                    ProductosAlmacen, Responsables, Tallas,
                                    Publicaciones)
from apps.compras.models import DetalleRecepcionCompra
from apps.website.models import DespachoMercancia, Ventas, DetalleVentas

from apps.compras.Clases.report import JasperReportMostrar
from django.utils.translation import ugettext_lazy as _
from django.utils.dateparse import parse_date
from django.contrib.messages.views import SuccessMessageMixin


# Vistas para CRUD del modelo Almacen
class AlmacenListar(ListView):
    """Vista para listar almacenes"""
    model = Almacenes
    fields = ['descripcion', 'id_responsable', 'activo']
    template_name = 'almacenes_listar.html'
    paginate_by = 10
    context_object_name = 'almacenes'
    queryset = Almacenes.objects.select_related(
        'id_responsable').all().order_by('id_almacen')


class AlmacenCrear(SuccessMessageMixin, CreateView):
    """Vista para crear almacenes"""
    model = Almacenes
    template_name = 'almacenes_crear.html'
    success_url = reverse_lazy('inventario:listar_almacenes')
    form_class = AlmacenForm
    second_form_class = ResponsableForm
    success_message = _('Almacen %(descripcion)s creado correctamente')

    def get_context_data(self, **kwargs):
        """Genera 2 formularios en la misma vista"""
        context = super(AlmacenCrear, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(self.request.GET)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(self.request.GET)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        form2 = self.second_form_class(request.POST)
        if form.is_valid() and form2.is_valid():
            almacen = form.save(commit=False)
            almacen.id_responsable = form2.save()
            almacen.activo = True
            almacen.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(form=form, form2=form2))


class AlmacenModificar(SuccessMessageMixin, UpdateView):
    model = Almacenes
    second_model = Responsables
    form_class = AlmacenForm
    second_form_class = ResponsableForm
    template_name = 'almacenes_modificar.html'
    success_url = reverse_lazy('inventario:listar_almacenes')
    success_message = _('Almacen %(descripcion)s actualizado correctamente')

    def get_context_data(self, **kwargs):
        context = super(AlmacenModificar, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk', 0)
        almacen = self.model.objects.get(id_almacen=pk)
        responsable = self.second_model.objects.get(
            id_responsable=almacen.id_responsable_id)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=responsable)
        context['id_almacen'] = pk
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        id_almacen = kwargs['pk']
        almacen = self.model.objects.get(id_almacen=id_almacen)
        responsable = self.second_model.objects.get(
            id_responsable=almacen.id_responsable_id)
        form = self.form_class(request.POST, instance=almacen)
        form2 = self.second_form_class(request.POST, instance=responsable)
        if form.is_valid() and form2.is_valid():
            form.save()
            form2.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return HttpResponseRedirect(
                self.get_context_data(form=form, form2=form2))


class AlmacenEliminar(DeleteView):
    model = Almacenes
    template_name = 'almacenes_eliminar.html'
    success_url = reverse_lazy('inventario:listar_almacenes')


# Vistas para CRUD del modelo Categorias
class CategoriaModificar(UpdateView):
    model = Categorias
    form_class = CategoriaForm
    template_name = 'categorias_modificar.html'
    success_url = reverse_lazy('inventario:crear_categorias')


class CategoriaEliminar(DeleteView):
    model = Categorias
    template_name = 'categorias_eliminar.html'
    success_url = reverse_lazy('inventario:crear_categorias')


class CategoriasCrear(ListView):
    template_name = 'categorias_crear.html'
    context_object_name = 'categorias_padres'
    queryset = Categorias.objects.filter(
        id_categoria_padre=1).order_by('id_categoria')


def get_category_children(request):
    """Obtiene una lista de categorias dado un padre"""
    # si no es una peticion ajax, devolvemos error 400
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    # definimos el termino de busqueda
    padre = request.POST['categoria']
    hijos = Categorias.objects.filter(id_categoria_padre=padre)

    prod_fields = ('descripcion', )

    data = serializers.serialize('json', hijos, fields=prod_fields)
    return HttpResponse(data, content_type="application/json")


def save_category(request):
    """Guarda la categoria"""
    # si no es una peticion ajax, devolvemos error 400
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest('Peticion incorrecta')
    try:
        padre = request.POST.get('id_padre', None)
        descripcion = request.POST.get('descripcion', None)
        categoria = Categorias()
        categoria.descripcion = descripcion
        categoria.id_categoria_padre = Categorias.objects.get(pk=padre)
        categoria.save()
        return JsonResponse({
            'mensaje': 'exito',
            'categoria_nueva': categoria.pk
        })
    except BaseException as exception:
        return JsonResponse({
            'mensaje':
            'error',
            'respuesta':
            _('Ocurrió un error: {}'.format(exception))
        })


def get_info_producto(request):
    """Obtiene datos del producto seleccionado"""
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    # definimos el termino de busqueda
    producto_id = request.POST['id_producto']
    almacen = request.POST['id_almacen']
    producto = Productos.objects.get(pk=producto_id)
    producto_almacen = ProductosAlmacen.objects.get(
        id_producto=producto_id, id_almacen=almacen)

    prod_fields = ('precio_proveedor', )

    data = serializers.serialize('json', producto, fields=prod_fields)
    return HttpResponse(data, content_type="application/json")


class GroupConcat(Aggregate):
    """Permite el uso de la función group_concat en el ORM"""

    function = 'GROUP_CONCAT'

    def as_postgresql(self, compiler, connection):
        """En PostgreSQL la función tiene otro nombre"""
        return self.as_sql(compiler, connection, function='ARRAY_AGG')


def get_lote_productos(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()
    lote = request.POST.get('id_recepcion', None)
    productos_en_lote = DetalleRecepcionCompra.objects.filter(
        id_recepcion=lote).values('id_producto', 'precio').distinct()
    if productos_en_lote.exists():
        lote_productos = DetalleRecepcionCompra.objects.filter(
            id_recepcion=lote).values('id_producto', 'color').annotate(
                tallas=GroupConcat('talla'),
                colores=GroupConcat('nombre_color'),
                codigo_color=GroupConcat('color'),
                cantidad=GroupConcat(
                    Cast('cantidad', CharField(max_length=10)))).values(
                        'id_producto', 'tallas', 'colores', 'codigo_color',
                        'cantidad').order_by('id_producto')
        json_productos = []
        for detalle in productos_en_lote:
            producto = Productos.objects.select_related().get(
                id_producto=detalle['id_producto'])
            un_producto = {}
            un_producto['cod_prod'] = detalle['id_producto']
            un_producto['nombre_prod'] = producto.descripcion
            un_producto['marca'] = producto.id_marca.descripcion
            un_producto['precio_venta'] = detalle['precio']
            cadena = producto.atributos
            atributos_producto = json.loads(cadena.replace("'", ""))
            un_producto['rutas_imagenes'] = [
                ruta['ruta_imagen'] for ruta in atributos_producto
            ]
            list_colores = []
            for item in lote_productos:
                if item['id_producto'] == detalle['id_producto']:
                    dict_caracteristica = {}
                    dict_caracteristica['color'] = item['colores'][0]
                    dict_caracteristica['codigo_color'] = item['codigo_color'][
                        0]
                    dict_caracteristica['tallas'] = item['tallas']
                    dict_caracteristica['cantidad'] = [
                        int(cantidad) for cantidad in item['cantidad']
                    ]
                    list_colores.append(dict_caracteristica)
            un_producto['caracteristicas'] = list_colores
            json_productos.append(un_producto)
        un_producto['mensaje'] = 'exito'
        un_producto['descripcion'] = 'listado de  productos de este lote'
        return JsonResponse(json_productos, safe=False)
    else:
        return JsonResponse({
            'mensaje': 'error',
            'descripcion': 'no se encontró el lote'
        })


def get_talla_productos(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()
    grupo_talla = request.POST.get('grupo', None)
    tallas_productos = Tallas.objects.filter(id_grupo=grupo_talla)

    talla_fields = ('descripcion', )

    data = serializers.serialize('json', tallas_productos, fields=talla_fields)
    return HttpResponse(data, content_type="application/json")


# Vistas para CRUD del modelo Productos
class ProductoListar(ListView):
    model = Productos
    template_name = 'productos_listar.html'
    paginate_by = 10
    context_object_name = 'productos'
    queryset = Productos.objects.all().order_by('id_producto')


def procesar_archivos(archivo, cod_proveedor):
    hash_archivo = str(uuid.uuid4().hex) + archivo.name[-4:]
    ruta = ''
    try:
        ruta = os.path.join(settings.MEDIA_ROOT, 'catalogo', cod_proveedor)
        os.makedirs(ruta, exist_ok=True)
    except OSError as e:
        if e.errno == 17:  #el directorio ya existe
            pass
        else:
            print('Ha ocurrido un error: {}'.format(e))
    archivo_disco = open(os.path.join(ruta, hash_archivo), 'wb+')
    contenido = ContentFile(archivo.read())

    for chunk in contenido.chunks():
        archivo_disco.write(chunk)
    archivo_disco.close()
    return os.path.join(ruta, hash_archivo).replace(settings.MEDIA_ROOT, '')


class ProductoCrear(SuccessMessageMixin, CreateView):
    model = Productos
    template_name = 'productos_crear.html'
    success_url = reverse_lazy('inventario:listar_productos')
    form_class = ProductoForm
    success_message = _('Producto %(descripcion)s creado correctamente')

    def get_context_data(self, **kwargs):
        context = super(ProductoCrear, self).get_context_data(**kwargs)
        context['categorias_padre'] = Categorias.objects.filter(
            id_categoria_padre=1)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():
            producto = form.save(commit=False)
            categoria_seleccionada = request.POST.get('id_cat_hijaD', None)
            if not categoria_seleccionada:
                categoria_seleccionada = request.POST.get('id_cat_hija', None)
                if not categoria_seleccionada:
                    categoria_seleccionada = request.POST.get('id_cat', None)
            producto.id_categoria = Categorias.objects.get(
                pk=categoria_seleccionada)
            json_atributos = []
            archivos = []
            for file_uploaded in request.FILES.getlist('imagen'):
                archivos.append(
                    procesar_archivos(file_uploaded,
                                      request.POST.get('id_proveedor', None)))
            todas_tallas = request.POST.getlist('atributos_prod', None)[0]
            otras_tallas = json.loads(todas_tallas)
            for talla, arch in zip(otras_tallas, archivos):
                talla['ruta_imagen'] = arch
                json_atributos.append(talla)
            producto.atributos = json.dumps(json_atributos)
            producto.save()
            return HttpResponseRedirect(
                reverse_lazy('inventario:listar_productos'))
        else:
            HttpResponse("Error")


class ProductoModificar(SuccessMessageMixin, UpdateView):
    model = Productos
    form_class = ProductoForm
    template_name = 'productos_modificar.html'
    success_url = reverse_lazy('inventario:listar_productos')
    success_message = _('Producto %(descripcion)s actualizado correctamente')

    def get_context_data(self, **kwargs):
        context_data = super(ProductoModificar, self).get_context_data(
            **kwargs)
        lista_cat_padres = Categorias.get_ancestors(self.object.id_categoria)
        lista_cat_padres.reverse()
        context_data['categorias_padre'] = Categorias.objects.filter(
            id_categoria_padre=1)
        categorias_seleccionadas = {}
        categorias_seleccionadas[
            'detalle'] = self.object.id_categoria.pk if len(
                lista_cat_padres) > 4 else 0
        categorias_seleccionadas['subcat'] = lista_cat_padres[
            3].id_categoria if len(
                lista_cat_padres) > 3 else self.object.id_categoria.pk
        categorias_seleccionadas['categoria'] = lista_cat_padres[
            2].id_categoria if len(lista_cat_padres) > 2 else 0
        categorias_seleccionadas['departamento'] = lista_cat_padres[
            1].id_categoria if len(lista_cat_padres) > 1 else 0
        context_data['cat'] = categorias_seleccionadas
        context_data['json_attrib'] = self.object.atributos
        return context_data

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        form = self.form_class(request.POST)
        if form.is_valid():
            producto = Productos.objects.get(id_producto=self.kwargs['pk'])
            producto.descripcion = form.cleaned_data['descripcion']
            producto.id_proveedor = form.cleaned_data['id_proveedor']
            producto.id_marca = form.cleaned_data['id_marca']
            producto.precio_proveedor = form.cleaned_data['precio_proveedor']
            categoria_seleccionada = request.POST.get('id_cat_hijaD', None)
            if not categoria_seleccionada:
                categoria_seleccionada = request.POST.get('id_cat_hija', None)
                if not categoria_seleccionada:
                    categoria_seleccionada = request.POST.get('id_cat', None)
            producto.id_categoria = Categorias.objects.get(
                pk=categoria_seleccionada)
            json_atributos = []
            archivos = []
            lista_archivos = []
            if request.FILES.getlist('imagen'):
                for file_uploaded in request.FILES.getlist('imagen'):
                    archivos.append(
                        procesar_archivos(file_uploaded,
                                          request.POST.get(
                                              'id_proveedor', None)))
            else:
                lista_archivos = [
                    json.loads(item)
                    for item in request.POST.getlist('old_atributos', None)
                ]
                for item in lista_archivos:
                    archivos.append(item[0]['ruta_imagen'])
            todas_tallas = request.POST.getlist('atributos_prod', None)[0]
            otras_tallas = json.loads(todas_tallas)
            for talla, arch in zip(otras_tallas, archivos):
                talla['ruta_imagen'] = arch
                json_atributos.append(talla)
            producto.atributos = json.dumps(json_atributos)
            producto.save()
            return HttpResponseRedirect(
                reverse_lazy('inventario:listar_productos'))
        else:
            return self.form_invalid(form)


class ProductoEliminar(DeleteView):
    model = Productos
    template_name = 'productos_eliminar.html'
    success_url = reverse_lazy('inventario:listar_productos')


# Vistas para CRUD del modelo Marcas
class MarcaListar(ListView):
    model = Marcas
    template_name = 'marcas_listar.html'
    paginate_by = 10
    context_object_name = 'marcas'


class MarcaCrear(SuccessMessageMixin, CreateView):
    model = Marcas
    template_name = 'marcas_crear.html'
    success_url = reverse_lazy('inventario:listar_marcas')
    form_class = MarcaForm
    success_message = _('Marca %(descripcion)s creada correctamente')

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class(request.POST)
        if form.is_valid():
            marca = form.save(commit=False)
            marca.activo = True
            marca.save()
            return HttpResponseRedirect(
                reverse_lazy('inventario:listar_marcas'))
        else:
            return self.render_to_response(self.get_context_data(form=form))


class MarcaModificar(SuccessMessageMixin, UpdateView):
    model = Marcas
    form_class = MarcaForm
    template_name = 'marcas_modificar.html'
    success_url = reverse_lazy('inventario:listar_marcas')
    success_message = _('Marca %(descripcion)s actualizada correctamente')


class MarcaEliminar(DeleteView):
    model = Marcas
    template_name = 'marcas_eliminar.html'
    success_url = reverse_lazy('inventario:listar_marcas')


class TallaListar(ListView):
    model = Tallas
    template_name = 'tallas_listar.html'
    paginate_by = 10
    context_object_name = 'tallas'


class TallaCrear(SuccessMessageMixin, CreateView):
    model = Tallas
    template_name = 'tallas_crear.html'
    success_url = reverse_lazy('inventario:listar_tallas')
    form_class = TallaForm
    success_message = _('Talla %(descripcion)s creada correctamente')


class TallaModificar(SuccessMessageMixin, UpdateView):
    model = Tallas
    form_class = TallaForm
    template_name = 'tallas_modificar.html'
    success_url = reverse_lazy('inventario:listar_tallas')
    success_message = _('Talla %(descripcion)s actualizada correctamente')


class TallaEliminar(DeleteView):
    model = Tallas
    template_name = 'tallas_eliminar.html'
    success_url = reverse_lazy('inventario:listar_tallas')


# Vistas publicar producto
class Publicar(SuccessMessageMixin, CreateView):
    model = Publicaciones
    template_name = 'publicar.html'
    success_url = reverse_lazy('inventario:listar_marcas')
    form_class = PublicacionForm
    success_message = _('Publicacion %(descripcion)s creada correctamente')

    def get_context_data(self, **kwargs):
        context = super(Publicar, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object
        publicacion_form = self.form_class(request.POST)
        prod_seleccionado = request.POST.getlist('seleccionado[]', None)
        id_recepcion = request.POST.get('id_lote', None)
        diccionario = {}
        if publicacion_form.is_valid():
            prod_seleccionado.sort()
            prod_seleccionado = [(producto.split('-'))
                                 for producto in prod_seleccionado]
            for clave, valor in prod_seleccionado:
                diccionario.setdefault(clave, []).append(valor)
            for id_producto, colores in diccionario.items():
                publicacion = publicacion_form.save(commit=False)
                publicacion.id_producto = Productos.objects.get(
                    id_producto=id_producto)
                publicacion.colores = colores
                publicacion.id_lote = id_recepcion
                publicacion.estado = 'E'
                publicacion.save()
            return HttpResponseRedirect(
                reverse_lazy('inventario:publicar_producto'))
        else:
            return self.form_invalid(publicacion_form)


@csrf_exempt
def cargar_almacenes(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    al = Almacenes.objects.filter(activo=True)

    al_fields = ('descripcion', )

    data = serializers.serialize('json', al, fields=al_fields)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def ExistenciaListar(request):
    if request.method == "POST":
        almacen = int(request.POST['almacenes'])
        existencia = int(request.POST['ex'])
        publicado = int(request.POST['pu'])
        listado = ProductosAlmacen.objects.all()
        if almacen > 0:
            listado = listado.filter(id_almacen=almacen)
        if existencia == 1:
            listado = listado.filter(existencia__gte=0)
        if existencia == 2:
            listado = listado.filter(existencia__lte=0)
        if publicado == 1:
            listado = listado.filter(publicado=True)
        if publicado == 2:
            listado = listado.filter(publicado=False)
    else:
        listado = ProductosAlmacen.objects.all()

    page = request.GET.get('page', 1)

    paginator = Paginator(listado, 20)
    try:
        p = paginator.page(page)
    except PageNotAnInteger:
        p = paginator.page(1)
    except EmptyPage:
        p = paginator.page(paginator.num_pages)

    return render(request, 'existencia_listar.html',
                  {'lista': listado,
                   'page': p})


# Vistas para CRUD de despacho de pedidos
class PredespachoListar(ListView):
    model = Ventas
    template_name = 'predespacho_listar.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = Ventas.objects.filter(estado="Pendiente")
        return queryset


@csrf_exempt
def DespachoCrear(request, id_venta):
    if request.is_ajax():
        venta = Ventas.objects.get(id_venta=id_venta)
        despacho = DespachoMercancia(
            id_venta=venta,
            courier=request.POST['courier'],
            estado="Predespacho")
        despacho.save()
        venta.estado = "Procesando"
        venta.save()
        return HttpResponse(
            json.dumps({
                'mensaje': 'Pre-despacho creado.',
                'id': despacho.id_despacho
            }),
            content_type="application/json")
    else:
        venta = Ventas.objects.get(id_venta=id_venta)
        detalle = DetalleVentas.objects.filter(id_venta=id_venta)
        return render(request, 'predespacho_crear.html',
                      {'venta': venta,
                       'detalle': detalle})

    return render(request, 'predespacho_crear.html')


class DespachoListar(ListView):
    model = DespachoMercancia
    template_name = 'despacho_listar.html'
    paginate_by = 20

    def get_queryset(self):
        queryset = DespachoMercancia.objects.filter(estado="Predespacho")
        return queryset


@csrf_exempt
def DespachoProcesar(request, id_despacho):
    if request.is_ajax():
        despacho = DespachoMercancia.objects.get(
            id_despacho=request.POST['id'])
        venta = Ventas.objects.get(id_venta=despacho.id_venta.id_venta)
        despacho.nro_tracking = request.POST['track']
        despacho.fecha_despacho = request.POST['fecha_despacho']
        despacho.estado = "Despachado"
        despacho.save()
        venta.estado = "Enviada"
        venta.save()
        return HttpResponse(
            json.dumps({
                'mensaje': 'Se ha procesado el despacho exitosamente.'
            }),
            content_type="application/json")
    else:
        despacho = DespachoMercancia.objects.get(id_despacho=id_despacho)
        detalle = DetalleVentas.objects.filter(id_venta=despacho.id_venta)
        return render(request, 'despacho_procesar.html',
                      {'despacho': despacho,
                       'detalle': detalle})

    return render(request, 'despacho_procesar.html')


@csrf_exempt
def cargar_categorias(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    cat = Categorias.objects.filter(id_categoria_padre=1)

    cat_fields = ('descripcion', )

    data = serializers.serialize('json', cat, fields=cat_fields)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def ConteoListar(request):
    if request.method == "POST":
        lis = []
        almacen = int(request.POST['almacenes'])
        categoria = int(request.POST['categorias'])
        listado = ProductosAlmacen.objects.all()
        lista = ProductosAlmacen.objects.all()

        if categoria > 0:
            cates = Categorias.get_all_children(
                Categorias.objects.get(id_categoria=categoria))
            for y in cates:
                lis.append(y.id_categoria)
            for x in lista:
                if x.id_producto.id_categoria.id_categoria not in lis:
                    listado = listado.filter(~Q(
                        id_producto=x.id_producto.id_producto))
        if almacen > 0:
            listado = listado.filter(id_almacen=almacen)
    else:
        listado = ProductosAlmacen.objects.all()
        almacen = 0
        categoria = 0

    page = request.GET.get('page', 1)

    paginator = Paginator(listado, 20)
    try:
        p = paginator.page(page)
    except PageNotAnInteger:
        p = paginator.page(1)
    except EmptyPage:
        p = paginator.page(paginator.num_pages)
    return render(request, 'conteo_listar.html', {
        'lista': listado,
        'almacen': almacen,
        'categoria': categoria,
        'page': p
    })


@csrf_exempt
def ConteoCargar(request, almacen, categoria):
    lis = []
    listado = ProductosAlmacen.objects.all().select_related('id_producto')
    lista = ProductosAlmacen.objects.all()

    if int(categoria) > 0:
        cates = Categorias.get_all_children(
            Categorias.objects.get(id_categoria=categoria))
        for y in cates:
            lis.append(y.id_categoria)
        for x in lista:
            if x.id_producto.id_categoria.id_categoria not in lis:
                listado = listado.filter(~Q(
                    id_producto=x.id_producto.id_producto))
        if int(almacen) > 0:
            listado = listado.filter(id_almacen=almacen)

    page = request.GET.get('page', 1)

    paginator = Paginator(listado, 20)
    try:
        p = paginator.page(page)
    except PageNotAnInteger:
        p = paginator.page(1)
    except EmptyPage:
        p = paginator.page(paginator.num_pages)
    return render(request, 'conteo_cargar.html', {'lista': listado, 'page': p})


@csrf_exempt
def ConteoGuardar(request):
    if not request.is_ajax() or request.method != "POST":
        return HttpResponseBadRequest()

    id = request.POST['id_lote']
    conteo = request.POST['contado']

    existencia = ProductosAlmacen.objects.get(id_lote=id)
    existencia.contado = conteo
    existencia.antes = existencia.existencia
    existencia.despues = conteo
    existencia.existencia = conteo
    existencia.save()

    return HttpResponse(
        json.dumps({
            'mensaje': 'bien'
        }), content_type="application/json")


@csrf_exempt
def BalanceFiltros(request):
    return render(request, 'balance_ingreso.html')


class MostrarReporteDespacho(JasperReportMostrar):

    report_name = 'despacho'

    def __init__(self, despacho):
        self.despacho = despacho
        self.filename = 'orden_despacho_{}'.format(self.despacho.id_despacho)
        super(MostrarReporteDespacho, self).__init__()

    def get_params(self):
        return {'id': self.despacho.id_despacho}


class MostrarReportePorDespachar(JasperReportMostrar):

    report_name = 'por_despachar'

    def __init__(self, estado):
        self.estado = estado
        self.filename = 'listado_ventas_pendientes'
        super(MostrarReportePorDespachar, self).__init__()

    def get_params(self):
        return {'estado': self.estado}


def mostrar_pdf_despacho(request, id_despacho):
    despacho = DespachoMercancia.objects.get(id_despacho=id_despacho)
    report = MostrarReporteDespacho(despacho)
    return report.render_to_response()


def mostrar_pdf_porDespachar(request):
    report = MostrarReportePorDespachar("Pendiente")
    return report.render_to_response()


class MostrarReporteConteo(JasperReportMostrar):
    report_name = 'conteo'

    def __init__(self, almacen, categoria):
        self.almacen = almacen
        self.categoria = categoria
        self.filename = 'listado_conteo'
        super(MostrarReporteConteo, self).__init__()

    def get_params(self):
        return {'almacen': self.almacen, 'categoria': self.categoria}


class MostrarReporteConteoAlm(JasperReportMostrar):
    report_name = 'conteo_alm'

    def __init__(self, almacen, categoria):
        self.almacen = almacen
        self.categoria = categoria
        self.filename = 'listado_conteo'
        super(MostrarReporteConteoAlm, self).__init__()

    def get_params(self):
        return {'almacen': self.almacen, 'categoria': self.categoria}


class MostrarReporteConteoCate(JasperReportMostrar):
    report_name = 'conteo_cate'

    def __init__(self, categoria, nombre):
        self.categoria = categoria
        self.nombre_cate = nombre
        self.filename = 'listado_conteo'
        super(MostrarReporteConteoCate, self).__init__()

    def get_params(self):
        return {'hijas': self.categoria, 'nombre_cate': self.nombre_cate}


class MostrarReporteConteoFiltro(JasperReportMostrar):
    report_name = 'conteo_filtro'

    def __init__(self, almacen, categoria, nombre):
        self.almacen = almacen
        self.categoria = categoria
        self.nombre_cate = nombre
        self.filename = 'listado_conteo'
        super(MostrarReporteConteoFiltro, self).__init__()

    def get_params(self):
        return {
            'almacen': self.almacen,
            'hijas': self.categoria,
            'nombre_cate': self.nombre_cate
        }


def mostrar_pdf_conteo(request, almacen, categoria):
    al = int(almacen)
    cat = int(categoria)
    if al == 0 and cat == 0:
        report = MostrarReporteConteo("Todos", "todas")
    elif al > 0 and cat == 0:
        report = MostrarReporteConteoAlm(almacen, "todas")
    elif cat > 0 and al == 0:
        lis = []
        cates = Categorias.get_all_children(
            Categorias.objects.get(id_categoria=categoria))
        for y in cates:
            lis.append(y.id_categoria)
        nombre = Categorias.objects.get(id_categoria=cat)
        report = MostrarReporteConteoCate(lis, nombre.descripcion)
    else:
        lis = []
        cates = Categorias.get_all_children(
            Categorias.objects.get(id_categoria=categoria))
        for y in cates:
            lis.append(y.id_categoria)
        nombre = Categorias.objects.get(id_categoria=cat)
        report = MostrarReporteConteoFiltro(almacen, lis, nombre.descripcion)

    return report.render_to_response()


class MostrarReporteBalance(JasperReportMostrar):
    report_name = 'balance_ingreso'

    def __init__(self, fecha_inicio, fecha_fin):
        self.f1 = parse_date(fecha_inicio)
        self.f2 = parse_date(fecha_fin)
        self.filename = 'balance_ingreso'
        super(MostrarReporteBalance, self).__init__()

    def get_params(self):
        return {'fecha1': self.f1, 'fecha2': self.f2}


class Departamento(ListView):
    model = Categorias
    template_name = 'departamentos_listado.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(Departamento, self).get_context_data(**kwargs)
        if 'titulo' not in context:
            context['titulo'] = _('Listado de departamentos')

        return context

    def get_queryset(self):
        return Categorias.objects.filter(
            id_categoria_padre=1).order_by("id_categoria")


@csrf_exempt
def guardar_deparpamento(request):
    if request.method == 'POST':
        guardo = False
        if request.POST['accion'] == 'guardar':
            form = DepartamentosForm(request.POST)
            if form.is_valid():
                form.save()
                guardo = True

        elif request.POST['accion'] == 'actualizar':
            categoria = request.POST["id_categoria"]
            form = DepartamentosForm(request.POST, \
            instance=Categorias.objects.get(id_categoria=categoria))
            if form.is_valid():
                form.save()
                guardo = True

        if not guardo:
            return HttpResponse(
                json.dumps({
                    'mensaje':
                    'Disculpe falló el registro del departamento'
                }),
                content_type="application/json")
        else:
            return HttpResponse(
                json.dumps({
                    'mensaje': 'exito'
                }),
                content_type="application/json")


@csrf_exempt
def buscar_deparpamento(request):
    if request.method == 'POST':
        data = Categorias.objects.get(
            id_categoria=request.POST["departamento"])
        return HttpResponse(
            json.dumps({
                "descripcion": str(data.descripcion),
                "id": str(data.id_categoria)
            }),
            content_type="application/json")


@csrf_exempt
def BalanceMostrar(request, fecha_inicio, fecha_fin):
    report = MostrarReporteBalance(fecha_inicio, fecha_fin)
    return report.render_to_response()