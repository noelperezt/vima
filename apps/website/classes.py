"""
    Clases y metodos para carro de compra.
"""
#TODO: validar carro con sesion y cargarlo
from base64 import b64encode
from os import urandom
from decimal import Decimal
from apps.website.models import CartItem

class Cart():
    """Estado del carro de compra

       atributos:

       ``items``: Lista de ``CartItem``

        ``subtotal``: subtotal de los ítems en el carro
    """
    def __init__(self, request):
        id_cart = obtener_id_carro(request)
        self.items = CartItem.objects.all().filter(cart_id=id_cart)
        self.subtotal = Decimal('0.00')
        for item in self.items:
            self.subtotal += Decimal(item.total())

def genera_id_carro():
    """Genera un id para el carro de compra."""
    return b64encode(urandom(36)).decode('utf-8')

def obtener_id_carro(request):
    """Devuelve el id_carro de la sesion del usuario."""
    if request.session.get('id_carro', '') == '':
        request.session['id_carro'] = genera_id_carro()
    return request.session['id_carro']

def obtener_items_carro(request):
    """Devuelve los items en el carro del usuario actual."""
    return CartItem.objects.select_related('producto').filter(cart_id=obtener_id_carro(request))

def obtener_cantidad_items_carro(request):
    """Devuelve la cantidad de items  en el carro del usuario actual."""
    return obtener_items_carro(request).count()

def crear_carro(request):
    """crear el carro de compras"""
    return Cart(request)
