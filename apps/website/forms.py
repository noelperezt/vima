""" Formularios de la aplicación website"""
from django import forms
from django.contrib.auth.models import User
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _

from apps.website.models import Cliente, CartItem, DetalleVentas, Ventas, DireccionesCliente
from apps.cuentas_pagar.models import Proveedores
from apps.cuentas_pagar.forms import ProveedorForm

CLIENTE_FORMSET = inlineformset_factory(
    User,
    Cliente,
    fields=['pais', 'documento_identidad', 'telefono'],
    can_delete=False,
    widgets={
        'pais': forms.TextInput(attrs={'class': 'form-control', 'nombre_etiqueta':'Pais'}),
        'documento_identidad': forms.TextInput(attrs={'class':
                                                      'form-control', 'nombre_etiqueta':'Documento de Identificación (DNI)'}),
        'telefono': forms.TextInput(attrs={'class': 'form-control', 'nombre_etiqueta':'Teléfono'}),
    })

PROVEEDOR_FORMSET = inlineformset_factory(
    User, Proveedores, form=ProveedorForm, can_delete=False)


class AgregarItemsCart(forms.ModelForm):
    """
        Formulario para agregar items al carro de compra.
    """

    class Meta:
        model = CartItem
        fields = ['cantidad', 'producto']
        cantidad = forms.IntegerField(
            widget=forms.TextInput(attrs={
                'size': '2',
                'value': '1',
                'class': 'form-control',
                'maxlength': '5'
            }),
            error_messages={'invalido': _('Ingrese una cantidad válida.')},
            min_value=1)
        producto_id = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        super(AgregarItemsCart, self).__init__(*args, **kwargs)

    def clean(self):
        if self.request:
            if not self.request.session.test_cookie_worked():
                raise forms.ValidationError(
                    _('Las cookies deben estar habilitadas en su navegador'))
            return self.cleaned_data


class DetalleCompraForm(forms.ModelForm):
    """
        Formulario de detalle de venta para el proceso de compra.
    """

    class Meta:
        model = DetalleVentas
        fields = [
            'id_producto', 'precio', 'talla', 'nombre_color', 'color',
            'cantidad'
        ]


class CompraForm(forms.ModelForm):
    """
        Formulario de detalle de venta para el proceso de compra.
    """

    class Meta:
        model = Ventas
        fields = ['id_cliente', 'estado', 'direccion_envio', 'monto_total']


class DireccionAdicional(forms.ModelForm):
    class Meta:
        model = DireccionesCliente
        fields = [
            'nombre', 'apellido', 'pais', 'ciudad', 'telefono',
            'zip_code', 'direccion'
        ]
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido': forms.TextInput(attrs={'class': 'form-control'}),
            'pais': forms.Select(attrs={'class': 'form-control'}),
            'ciudad': forms.Select(attrs={'class': 'form-control'}),
            'telefono': forms.Select(attrs={'class': 'form-control'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control'})
        }
