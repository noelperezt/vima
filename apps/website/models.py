"""
    Modelos de la aplicación website.
"""

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField
from apps.inventario.models import Productos, Publicaciones
from apps.cuentas_pagar.models import Paises, Ciudades


class Cliente(models.Model):
    """
        Definición de Cliente Vima.

        Basado en el modelo User de Django, agrega país, documento de identidad
        y teléfono a un usuario
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    pais = models.CharField(
        max_length=150,
        null=True,
        blank=True,
        default='',
        verbose_name=_('pais donde se encuentra el cliente'))
    documento_identidad = models.CharField(
        max_length=150,
        null=True,
        blank=True,
        default='',
        verbose_name=_('documento de identidad del cliente'))
    telefono = models.CharField(
        max_length=40,
        null=True,
        blank=True,
        default='',
        verbose_name=_('telefonos del cliente'))

    class Meta:
        db_table = 'clientes'
        verbose_name = _('cliente')
        verbose_name_plural = _('cliente')


class DireccionesCliente(models.Model):
    """
        Direcciones adicionales del cliente Vima.
    """
    id_direccion = models.AutoField(primary_key=True)
    id_cliente = models.ForeignKey(
        Cliente,
        null=True,
        blank=True,
        verbose_name=_('Identificador del cliente'))
    nombre = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        default='',
        verbose_name=_('nombre asociado con la dirección'))
    apellido = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        default='',
        verbose_name=_('apellido asociado con la dirección '))
    pais = models.ForeignKey(
        Paises,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default='',
        verbose_name=_('país de la dirección'))
    ciudad = models.ForeignKey(
        Ciudades,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=0,
        verbose_name=_('ciudad de la dirección')) 
    telefono = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        default='',
        verbose_name=_('teléfono asociado con la dirección'))
    zip_code = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        default='',
        verbose_name=_('direccion de envío del cliente'))
    direccion = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        default='',
        verbose_name=_('direccion de envío del cliente'))

    class Meta:
        db_table = 'direcciones_cliente'


class Ventas(models.Model):
    """
        Definición de una venta.
    """
    id_venta = models.AutoField(primary_key=True)
    id_cliente = models.ForeignKey(
        Cliente,
        null=True,
        blank=True,
        verbose_name=_('Identificador del cliente'))
    fecha = models.DateField(
        auto_now_add=True, editable=True, verbose_name=_('fecha de la venta'))
    estado = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        verbose_name=_('estado de la venta'))
    direccion_envio = models.CharField(
        max_length=300,
        null=True,
        blank=True,
        verbose_name=_('direccion de envío de esta venta'))
    monto_total = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.00,
        verbose_name=_('monto total de la venta'))

    class Meta:
        db_table = 'ventas'
        verbose_name = _('venta')
        verbose_name_plural = _('ventas')

    def __str__(self):
        return 'Venta #{} - Cliente:{}'.format(self.id_venta,
                                               self.id_cliente.id)


class DetalleVentas(models.Model):
    """
        Detalle de la venta
    """
    id_detalle = models.AutoField(primary_key=True)
    id_venta = models.ForeignKey(
        Ventas, null=True, blank=True, verbose_name=_('Venta relacionada'))
    id_producto = models.ForeignKey(
        Productos,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name=_('Producto'))
    precio = models.DecimalField(
        max_digits=19,
        decimal_places=3,
        default=0.00,
        verbose_name=_('precio de venta'))
    talla = models.CharField(
        max_length=20, null=False, blank=True, verbose_name=_('talla'))
    nombre_color = models.CharField(
        max_length=100,
        null=False,
        blank=True,
        verbose_name=_('nombre del color'))
    color = models.CharField(
        max_length=10, null=False, blank=True, verbose_name=_('color'))
    imagen = models.CharField(
        max_length=250,
        null=False,
        blank=True,
        verbose_name=_('ruta de la imagen'))
    cantidad = models.IntegerField(
        default=0,
        null=False,
        blank=True,
        verbose_name=_('cantidad en existencia'))

    class Meta:
        db_table = 'detalle_ventas'
        verbose_name = _('detalle de venta')
        verbose_name_plural = _('detalles de ventas')


class DespachoMercancia(models.Model):
    """
        Defiinición de Despacho de mercancia.
    """
    id_despacho = models.AutoField(primary_key=True)
    id_venta = models.ForeignKey(
        Ventas,
        null=True,
        blank=True,
        verbose_name=_('Identificador de la venta que se despacha'))
    fecha_despacho = models.DateField(
        editable=True, null=True, verbose_name=_('Fecha de despacho'))
    fecha_predespacho = models.DateField(
        auto_now_add=True,
        editable=True,
        null=True,
        verbose_name=_('Fecha de pre-despacho'))
    nro_tracking = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_('número de tracking del envío'))
    courier = models.CharField(
        max_length=250,
        null=True,
        blank=True,
        verbose_name=_('Descripcion del courier'))
    estado = models.CharField(
        max_length=20,
        null=True,
        blank=True,
        verbose_name=_('estado del despacho'))

    class Meta:
        db_table = 'despacho_mercancia'
        verbose_name = _('despacho de mercancia')
        verbose_name_plural = _('despachos de mercancia')


class CartItem(models.Model):
    """
        Item del carro de compra.

        Campos: ``cart_id``, ``fecha_agregado``, ``cantidad``, ``producto``
    """
    cart_id = models.CharField(max_length=50)
    fecha_agregado = models.DateTimeField(auto_now_add=True)
    cantidad = models.IntegerField(default=1)
    producto = models.ForeignKey(Publicaciones, unique=False)
    atributos = JSONField()

    class Meta:
        db_table = 'cart_items'
        ordering = ['fecha_agregado']

    def total(self):
        """Devuelve el total en el carro de compras"""
        return self.cantidad * self.producto.get_precio_venta()['precio_venta']

    def name(self):
        """Devuelve el nombre de un producto."""
        return self.producto.Descripcion

    def price(self):
        """Devuelve el precio de un producto."""
        return self.producto.get_precio_venta()['precio_venta']

    def get_absolute_url(self):
        """Devuelve la url de un producto."""
        return self.producto.get_absolute_url()

    def augment_quantity(self, cantidad):
        """Incrementa la cantidad de un producto en el carro."""
        self.cantidad = self.cantidad + int(cantidad)
        self.save()


class FavoritosCliente(models.Model):
    """
        Modelo de Productos Favoritos de Cliente.
    """
    id_favorito = models.AutoField(primary_key=True)
    id_cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    id_producto = models.ForeignKey(Publicaciones, on_delete=models.CASCADE)

    class Meta:
        db_table = 'favoritos_cliente'
        verbose_name = _('favorito clientes')
        verbose_name_plural = _('favoritos cliente')
