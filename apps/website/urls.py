"""
    Url's de la aplicación website
"""

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LogoutView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.views import (PasswordResetView, PasswordResetDoneView,
                                       PasswordResetConfirmView, PasswordResetCompleteView)

from apps.website.views import (
    ClienteInicioSession, ComprasLista, DashboardCliente, DesignerLounge,
    DesignerLoungeRegistro, DetallePublicacion, IndexWebsite, ProcesoCompra,
    ProveedorInicioSession, RegistroCliente, ShoppingCart,
    VistaFavoritosCliente, add_address_user, agregar_items_carro, add_favorito,
    VistaResultados)

urlpatterns = [
    url(r'^$', IndexWebsite.as_view(), name='index_website'),
    url(
        r'^designer_lounge/$',
        login_required(
            DesignerLounge.as_view(),
            login_url=reverse_lazy('website:inicio_sesion_proveedor')),
        name='designer_lounge'),
    url(
        r'^designer_lounge/registrar$',
        DesignerLoungeRegistro.as_view(),
        name='designer_lounge_registro'),
    url(
        r'^sesion_proveedor/$',
        ProveedorInicioSession.as_view(),
        name='inicio_sesion_proveedor'),
    url(
        r'^inicio_sesion/$',
        ClienteInicioSession.as_view(),
        name='inicio_sesion'),
    url(r'^cerrar_sesion/$', LogoutView.as_view(), name='cerrar_sesion'),
    url(
        r'^cerrar_sesion/(?P<next_page>.*)/$',
        LogoutView.as_view(),
        name='cerrar_sesion_next'),
    url(
        r'^registro_cliente/$',
        RegistroCliente.as_view(),
        name='registro_cliente'),

    url(
        r'^usuarios/reset/password_reset',
        PasswordResetView.as_view(), {
            'template_name': 'email_forgot.html',
            'email_template_name': 'correo_cambio.html'
        },
        name='forgot_password'),
    # url(
    #     r'^usuarios/password_reset_done',
    #     password_reset_done,
    #     {'template_name': 'confirmacion_olvido_password.html'},
    #     name='password_ready'),
    # url(
    #     r'^usuarios/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
    #     password_reset_confirm, {'template_name': 'cambiar_password.html'},
    #     name='confirm_password_reset'),
    # url(
    #     r'^usuarios/reset/done',
    #     password_reset_complete, {'template_name': 'password_changed.html'},
    #     name='complete_password_reset'),
    url(r'^carrito_compra/$', ShoppingCart.as_view(), name='carrito_compra'),
    url(
        r'^comprar/$',
        login_required(
            ProcesoCompra.as_view(),
            login_url=reverse_lazy('website:inicio_sesion')),
        name='proceso_compra'),
    url(r'^busqueda/$', VistaResultados.as_view(),name='busqueda-resultados'),
    url(
        r'^detalle/(?P<pk>\d+)$',
        DetallePublicacion.as_view(),
        name='detalle_publicacion'),
    url(
        r'^dashboard/(?P<pk>\d+)$',
        login_required(
            DashboardCliente.as_view(),
            login_url=reverse_lazy('website:inicio_sesion')),
        name='dashboard_cliente'),
    url(r'^compras/$', ComprasLista.as_view(), name='lista_compras'),
    url(
        r'^favoritos/$',
        login_required(
            VistaFavoritosCliente.as_view(),
            login_url=reverse_lazy('website:inicio_sesion')),
        name='favoritos_cliente'),
    url(r'^comprar/guardar_direccion/$',
        login_required(
            add_address_user,
            login_url=reverse_lazy('website:inicio_sesion'))),
    url(
        r'^guardar_favorito/$',
        login_required(
            add_favorito, login_url=reverse_lazy('website:inicio_sesion')),
        name='add_favoritos'),
    url(r'^agregar_items_carro/$', agregar_items_carro, name='add_to_cart'),
]
