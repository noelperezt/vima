"""
    Vistas de la aplicación website
"""
import ast
import datetime

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from django.http.response import JsonResponse
from django.shortcuts import HttpResponseRedirect, render
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (CreateView, DetailView, ListView,
                                  TemplateView, UpdateView)

from apps.administrador.forms import UserCreateForm, UserUpdateForm
from apps.cuentas_pagar.forms import ProveedorForm
from apps.cuentas_pagar.models import Proveedores, Ciudades
from apps.inventario.models import Publicaciones, Categorias
from apps.website.forms import (CLIENTE_FORMSET, AgregarItemsCart, CompraForm,
                                PROVEEDOR_FORMSET, DireccionAdicional)
from apps.website.models import (Cliente, DetalleVentas, DireccionesCliente,
                                 Ventas, FavoritosCliente, CartItem)

from apps.website.classes import Cart, obtener_id_carro, obtener_items_carro, crear_carro, obtener_cantidad_items_carro
from django.db import transaction, IntegrityError


class IndexWebsite(TemplateView):
    """
        Vista principal del website VIMA.
    """
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        """Agrega al contexto la lista de proveedores diseñadores y los últimos productos."""
        context = super(IndexWebsite, self).get_context_data(**kwargs)
        context['proveedores_disenadores'] = Proveedores.objects.filter(
            activo=True, es_disenador=True)
        context['ultimos_productos'] = Publicaciones.objects.select_related(
            'id_producto')[:10]
        context['cantidad_items_carro'] = obtener_cantidad_items_carro(
            self.request)

        tags = []
        for t in Categorias.objects.filter(id_categoria_padre=None):
            tags.append(t.serializable_object())

        arbol_categorias = {}
        for categoria in Categorias.objects.filter(
                id_categoria_padre=1).order_by('id_categoria'):
            desc_categoria = categoria.descripcion
            hijos = categoria.get_all_children(categoria.id_categoria)
            for hijo in hijos:
                if hijo.descripcion != desc_categoria:
                    arbol_categorias.setdefault(desc_categoria,
                                                []).append(hijo.descripcion)
                else:
                    arbol_categorias.setdefault(desc_categoria, []).append('')

        context['cat_padres'] = arbol_categorias
        return context


class DesignerLounge(TemplateView):
    """
        Vista principal del area de designers de VIMA.
    """
    template_name = 'home_proveedor.html'


class DesignerLoungeRegistro(SuccessMessageMixin, CreateView):
    """
        Vista de registro de proveedores de VIMA.
    """
    model = User
    template_name = 'registro_proveedor.html'
    success_url = reverse_lazy('website:designer_lounge')
    form_class = UserCreateForm
    success_message = _('Proveedor %(nombre_empresa)s creado correctamente')

    def get(self, request, *args, **kwargs):
        """Redefinición de metodo ``get()`` para manejar el perfil de cliente."""
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        proveedor_form = PROVEEDOR_FORMSET()
        return self.render_to_response(
            self.get_context_data(form=form, proveedor_form=proveedor_form))

    def post(self, request, *args, **kwargs):
        """Redefinición de metodo ``post()`` para manejar el perfil de cliente."""
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        proveedor_form = PROVEEDOR_FORMSET(request.POST, request.FILES)
        if form.is_valid() and proveedor_form.is_valid(): 
            return self.form_valid(form, proveedor_form)
        else:  
            return self.form_invalid(form, proveedor_form)

    def form_valid(self, form, proveedor_form):
        """Redefinición de metodo ``form_valid()`` para manejar el perfil de cliente."""
        try:
            with transaction.atomic():
                self.object = form.save(commit=False)
                self.object.is_staff = False
                self.object.save()
                proveedor_form.instance = self.object
                nuevo_proveedor = proveedor_form.save(commit=False)
                nuevo_proveedor[
                    0].ciudad_direccion_envio = Ciudades.objects.get(
                        id_ciudad=self.request.POST.get(
                            "id_proveedores-0-ciudad_direccion_envio"))
                nuevo_proveedor[
                    0].ciudad_direccion_fiscal = Ciudades.objects.get(
                        id_ciudad=self.request.POST.get(
                            "id_proveedores-0-ciudad_direccion_fiscal"))
                nuevo_proveedor[0].save()
                messages.add_message(self.request, messages.INFO, _('Su registro ha sido exitoso'))
                return HttpResponseRedirect(self.get_success_url())
        except IntegrityError:
            return super(DesignerLoungeRegistro, self).form_invalid(
                form, proveedor_form)

    def form_invalid(self, form, proveedor_form):
        """Redefinición de metodo ``form_invalid()`` para manejar el perfil de cliente."""
        return self.render_to_response(
            self.get_context_data(form=form, proveedor_form=proveedor_form))


class RegistroCliente(SuccessMessageMixin, CreateView):
    """
        Vista de registro de clientes de VIMA.
    """
    model = User
    template_name = 'registro_cliente.html'
    success_url = reverse_lazy('website:inicio_sesion')
    form_class = UserCreateForm
    success_message = _('Usuario %(username)s creado correctamente')

    def get(self, request, *args, **kwargs):
        """Redefinición de metodo ``get()`` para manejar el perfil de cliente."""
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        cliente_form = CLIENTE_FORMSET()
        return self.render_to_response(
            self.get_context_data(form=form, cliente_form=cliente_form))

    def post(self, request, *args, **kwargs):
        """Redefinición de metodo ``post()`` para manejar el perfil de cliente."""
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        cliente_form = CLIENTE_FORMSET(request.POST)
        if form.is_valid() and cliente_form.is_valid():
            return self.form_valid(form, cliente_form)
        else:
            return self.form_invalid(form, cliente_form)

    def form_valid(self, form, cliente_form):
        """Redefinición de metodo ``form_valid()`` para manejar el perfil de cliente."""
        self.object = form.save(commit=False)
        self.object.is_staff = False
        self.object.save()
        cliente_form.instance = self.object
        cliente_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, cliente_form):
        """Redefinición de metodo ``form_invalid()`` para manejar el perfil de cliente."""
        return self.render_to_response(
            self.get_context_data(form=form, cliente_form=cliente_form))


class ProveedorInicioSession(TemplateView):
    """
        Vista inicio de sesión de clientes VIMA.
    """
    template_name = 'inicio_sesion_designer_lounge.html'

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return render(request, 'home_proveedor.html')
        else:
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            print('user')
            #TODO: cambiar orden de los mensajes
            if user is not None:
                print('user not none')
                if user.is_active and not user.is_staff:
                    login(request, user)
                    print('logueaado')
                    if request.GET:
                        print('get')
                        return HttpResponseRedirect(request.GET.get('next'))
                    else:
                        print('redirect') 
                        return HttpResponseRedirect(
                            reverse_lazy(
                                'website:inicio_sesion_proveedor',
                                kwargs={'pk': user.id}))
                else:
                    print('inactivo')
                    messages.add_message(request, messages.INFO,
                                         _('Cuenta de usuario inactiva'))
            else:
                print('contraseña')
                messages.add_message(
                    request, messages.INFO,
                    _('Nombre de usuario o contraseña no válido'))
        return HttpResponseRedirect(
            reverse_lazy('website:inicio_sesion_proveedor'))


class ClienteInicioSession(TemplateView):
    """
        Vista inicio de sesión de clientes VIMA.
    """
    template_name = 'inicio_sesion_cliente.html'

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return render(request, 'Dashboard_Cliente.html')
        else:
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active and not user.is_staff:
                    login(request, user)
                    request.session['id_carro'] = obtener_id_carro(request)
                    if request.GET:
                        return HttpResponseRedirect(request.GET.get('next'))
                    else:
                        return HttpResponseRedirect(
                            reverse_lazy(
                                'website:dashboard_cliente',
                                kwargs={'pk': user.id}))
                else:
                    messages.add_message(request, messages.INFO,
                                         _('Cuenta de usuario inactiva'))
            else:
                messages.add_message(
                    request, messages.INFO,
                    _('Nombre de usuario o contraseña no válido'))
        return HttpResponseRedirect(reverse_lazy('website:inicio_sesion'))


class ShoppingCart(SuccessMessageMixin, CreateView):
    """
        Definición de item de carro de compra.
    """
    model = Ventas
    context_object_name = 'carro_compra'
    template_name = 'carrito.html'
    success_url = reverse_lazy('website:designer_lounge')
    form_class = CompraForm
    success_message = _('venta exitosa')

    def get(self, request, *args, **kwargs):
        """ Redefinir metodo get """
        carro_compra = crear_carro(request)
        return render(
            request,
            self.template_name,
            context={
                'user_carro': carro_compra,
                'carro_compra': obtener_items_carro(self.request)
            })


class DetallePublicacion(DetailView):
    """
        Vista detalle de la publicación de un producto.
    """
    model = Publicaciones
    template_name = 'detalle.html'
    context_object_name = 'detalle_producto'
    form_class = AgregarItemsCart

    def get_queryset(self):
        return Publicaciones.objects.select_related('id_producto').filter(
            id_publicacion=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(DetallePublicacion, self).get_context_data(**kwargs)
        lista_atributos = Publicaciones.objects.select_related(
            'id_producto').values('id_producto__atributos',
                                  'id_producto').filter(
                                      id_publicacion=self.kwargs['pk'])[0]
        colores = []
        nombre_colores = []
        tallas = []
        imagenes = []
        valores = ast.literal_eval(lista_atributos['id_producto__atributos'])
        for item in valores:
            colores.append(item['codigo_color'])
            nombre_colores.append(item['nombre_color'])
            tallas.append(item['tallas'])
            imagenes.append(item['ruta_imagen'])
        talla_final = [
            una_talla for lista_talla in tallas for una_talla in lista_talla
        ]
        context['colores'] = colores
        context['nombre_colores'] = nombre_colores
        context['tallas'] = talla_final
        context['imagenes'] = imagenes
        context['precio'] = Publicaciones.objects.get(
            id_publicacion=self.kwargs['pk']).get_precio_venta().get(
                'precio_venta')
        context['recomendados'] = Publicaciones.objects.all(
        )  #TODO: solo los recomendados
        return context

    #TODO: agrega items al carro
    def get(self, request, *args, **kwargs):
        super(DetallePublicacion, self).get(self, request, *args, **kwargs)
        publicacion = self.get_object()
        form = AgregarItemsCart(request=request)
        form.fields['producto'].widget.attrs[
            'value'] = publicacion.id_producto.id_producto
        request.session.set_test_cookie()
        return render(request, self.template_name, self.get_context_data())

    # def post(self, request, *args, **kwargs):
    #     form = AgregarItemsCart(request, request.POST)
    #     if form.is_valid():
    #         agregar_items_carro(request)
    #     if request.session.test_cookie_worked():
    #         request.session.delete_test_cookie()
    #     return HttpResponseRedirect(reverse_lazy('website:lista_compras'))


class DashboardCliente(SuccessMessageMixin, UpdateView):
    """
        Vista principal de Dashboard de cliente. Permite editar el perfil de usuario.
    """
    model = User
    template_name = 'Dashboard_Cliente.html'
    form_class = UserUpdateForm
    success_url = reverse_lazy('website:dashboard_cliente')
    success_message = _('Usuario %(username)s actualizado correctamente')

    def get(self, request, *args, **kwargs):
        """Redefinición de metodo ``get()`` para manejar el perfil de cliente."""
        self.object = User.objects.get(pk=kwargs['pk'])
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        cliente = Cliente.objects.get(user__id=kwargs['pk'])
        cliente_form = CLIENTE_FORMSET(self.object)
        return self.render_to_response(
            self.get_context_data(form=form, cliente_form=cliente_form))

    def post(self, request, *args, **kwargs):
        """Redefinición de metodo ``post()`` para manejar el perfil de cliente."""
        if request.user.is_authenticated() and request.user.id == kwargs['pk']:
            self.object = User.objects.get(pk=kwargs['pk'])
            form_class = self.get_form_class()
            form = self.get_form(form_class)
            cliente_form = CLIENTE_FORMSET(request.POST)
            if form.is_valid() and cliente_form.is_valid():
                return self.form_valid(form, cliente_form)
            else:
                return self.form_invalid(form, cliente_form)
        else:
            return self.form_invalid(form, cliente_form)

    def form_valid(self, form, cliente_form):
        """Redefinición de metodo ``form_valid()`` para manejar el perfil de cliente."""
        self.object = form.save()
        cliente_form.instance = self.object
        cliente_form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, cliente_form):
        """Redefinición de metodo ``form_invalid()`` para manejar el perfil de cliente."""
        return self.render_to_response(
            self.get_context_data(form=form, cliente_form=cliente_form))


class ComprasLista(ListView):
    """
        Vista de carro de compras
    """
    Model = DetalleVentas
    context_object_name = 'lista_productos'
    template_name = 'Dashboard_Compra.html'
    paginate_by = 3

    def get_queryset(self):
        print(
            DetalleVentas.objects.select_related('id_producto', 'id_venta')
            .filter(id_venta__id_cliente=self.request.user.id).query)
        return DetalleVentas.objects.all()


class ProcesoCompra(TemplateView):
    """Vista para proceso de compra"""
    template_name = 'proceso_compra.html'

    def get_context_data(self, **kwargs):
        """Modificar el contexto"""
        context = super(ProcesoCompra, self).get_context_data(**kwargs)
        context['items'] = Cart
        context['direciones_adicionales'] = DireccionesCliente.objects.filter(
            id_cliente=self.request.user.id)
        context['form_direccion'] = DireccionAdicional()
        context['carro_compra'] = crear_carro(self.request)
        context['carro_items'] = obtener_items_carro(self.request)

        return context

    def post(self, request, *args, **kwargs):
        """"""
        self.object = self.get_object()
        if request.POST.get('checkout', None):
            venta = Ventas()
            venta.id_cliente = request.user.id
            venta.fecha = datetime.date()
            venta.estado = 'P'
            venta.direccion_envio = request.POST.get('direccion')
            venta.save()
            carro = Cart(request)
            for items in carro.items:
                detalle_venta = DetalleVentas()
                detalle_venta.id_venta = venta
                detalle_venta.id_producto = items.id_publicacion
                detalle_venta.precio = items.price()
                detalle_venta.talla = ''
                detalle_venta.nombre_color = ''
                detalle_venta.color = ''
                detalle_venta.imagen = ''
                detalle_venta.cantidad = items.cantidad
                detalle_venta.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data())


def add_address_user(request):
    """Guarda direcciones adicionales del usuario"""
    # si no es una peticion ajax, devolvemos error 400
    if not request.is_ajax() or request.method != "POST":
        return JsonResponse({
            'mensaje': 'error',
            'respuesta': 'peticion incorrecta'
        })
    try:
        datos = request.POST.get('campos', None)
        usuario = request.user.id
        nueva_direccion = DireccionesCliente()
        nueva_direccion.id_cliente = Cliente.object.get(id_user=usuario)
        nueva_direccion.direccion = request.POST.get('direccion', None)
        nueva_direccion.save()
        return JsonResponse({
            'mensaje': 'exito',
            'respuesta': 'Dirección agregada correctamente'
        })
    except:
        return JsonResponse({
            'mensaje': 'error',
            'respuesta': 'No se pudo guardar la dirección'
        })


def add_favorito(request):
    """Guarda un producto en los favoritos del usuario"""
    # si no es una peticion ajax, devolvemos error 400
    if not request.is_ajax() or request.method != "POST":
        return JsonResponse({
            'mensaje': 'error',
            'respuesta': 'peticion incorrecta'
        })
    try:
        usuario = request.user.id
        favorito = FavoritosCliente()
        favorito.id_cliente = Cliente.objects.get(id=usuario)
        producto = request.POST.get('producto', None)
        favorito.id_producto = Publicaciones.objects.get(
            id_publicacion=producto)
        favorito.save()
        return JsonResponse({
            'mensaje':
            'exito',
            'respuesta':
            'Se ha agregado el producto a favoritos'
        })
    except BaseException as e:
        return JsonResponse({
            'mensaje': 'error',
            'respuesta': _('ocurrió un error: {}'.format(e))
        })


def agregar_items_carro(request):
    """Agrega items al carro de compra."""
    datos = request.POST.copy()
    id_publicacion = datos.get('id_publicacion', None)
    cantidad = datos.get('cantidad', 1)
    producto = Publicaciones.objects.get(id_publicacion=id_publicacion)
    productos_carro = obtener_items_carro(request)
    existe_producto = False
    if productos_carro:
        for item_carro in productos_carro:
            if item_carro.producto.id_producto == producto.id_producto:
                item_carro.augment_quantity(cantidad)
                existe_producto = True
    if not existe_producto:
        item = CartItem()
        item.producto = producto
        item.cantidad = cantidad
        item.cart_id = obtener_id_carro(request)
        item.atributos = '{}'
        item.save()
    return JsonResponse({
        'mensaje': 'exito',
        'respuesta': _('Se ha agregado al carro de compra')
    })


class VistaFavoritosCliente(ListView):
    """Vista para listado de productos favoritos del cliente
    """
    model = FavoritosCliente
    context_object_name = 'favoritos'
    template_name = 'Dashboard_Favoritos.html'


class VistaResultados(ListView):
    template_name = 'resultado.html'

    def get(self, request, *args, **kwargs):
        id_proveedor = request.GET.get('id_proveedor', None)

    def get_queryset(self):
        return Publicaciones.objects.filter(id_publicacion=1)


def get_categorias():
    padres = Categorias.objects.filter(id_categoria_padre=1)
