$(function () {
  setTimeout(function () { $("#idmessages").remove();}, 3000);

});

function mostrarMensajes(msj) {
  $('#msg', '#mensajes').html(msj);
  $('#mensajes').modal('show');
}

function soloNumeros(e) {
  var key = window.Event ? e.which : e.keyCode;
  return ((key >= 48 && key <= 57) || e.keyCode == 9 || e.keyCode == 46)
}

function str_pad(input, padLength, padString, padType) { // eslint-disable-line camelcase
  //  discuss at: http://locutus.io/php/str_pad/
  // original by: Kevin van Zonneveld (http://kvz.io)
  // improved by: Michael White (http://getsprink.com)
  //    input by: Marco van Oort
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: str_pad('Kevin van Zonneveld', 30, '-=', 'STR_PAD_LEFT')
  //   returns 1: '-=-=-=-=-=-Kevin van Zonneveld'
  //   example 2: str_pad('Kevin van Zonneveld', 30, '-', 'STR_PAD_BOTH')
  //   returns 2: '------Kevin van Zonneveld-----'
  var half = ''
  var padToGo
  var _strPadRepeater = function (s, len) {
    var collect = ''
    while (collect.length < len) {
      collect += s
    }
    collect = collect.substr(0, len)
    return collect
  }
  input += ''
  padString = padString !== undefined ? padString : ' '
  if (padType !== 'STR_PAD_LEFT' && padType !== 'STR_PAD_RIGHT' && padType !== 'STR_PAD_BOTH') {
    padType = 'STR_PAD_RIGHT'
  }
  if ((padToGo = padLength - input.length) > 0) {
    if (padType === 'STR_PAD_LEFT') {
      input = _strPadRepeater(padString, padToGo) + input
    } else if (padType === 'STR_PAD_RIGHT') {
      input = input + _strPadRepeater(padString, padToGo)
    } else if (padType === 'STR_PAD_BOTH') {
      half = _strPadRepeater(padString, Math.ceil(padToGo / 2))
      input = half + input + half
      input = input.substr(0, padLength)
    }
  }
  return input
}
function number_format(number, decimals, decPoint, thousandsSep) {
  decimals = decimals || 0;
  number = parseFloat(number);

  if (!decPoint || !thousandsSep) {
    decPoint = '.';
    thousandsSep = ',';
  }

  var roundedNumber = Math.round(Math.abs(number) * ('1e' + decimals)) + '';
  var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
  var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
  var formattedNumber = "";

  while (numbersString.length > 3) {
    formattedNumber += thousandsSep + numbersString.slice(-3)
    numbersString = numbersString.slice(0, -3);
  }

  return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
}
function validateDecimal(valor) {
  var valor = valor.value;
  var pattern = /^\d+(\.\d+)?$/; // cualquier cosa que no sea numero y punto;

  if (pattern.test(valor) == false) {
    return false;
    // realizar alguna accion si el campo no es valido;
    // por ejemplo, mostrar una imagen de invalido, mostrar un mensaje;
  }
}

function verificarNumero(evt) {
  // Espacio = 8, Enter = 13, '0′ = 48, '9′ = 57, '.' = 46
  var obj = evt.srcElement || evt.target;
  var key = (document.all) ? evt.which : evt.keyCode;
  var precio = obj.value;
  for (var i = 0; i < precio.length; i++) {
    if (precio.charAt(i) == '.') {
      if ((key == 8)) {
        return key;
      } else
        if ((key == 46) || precio.charAt(i + 3)) {
          return false;
        }
    }
  }
  return (key <= 13 || (key >= 48 && key <= 57) || key == 46);
}//Math.round10(1.005, -2)

function isValidEmail(mail) {
  var correo = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail);
  if (!correo) return false; else return true;
}

function fillCities(csrf_token, elem_select, country, selected_city) {
  $.ajax({
    dataType: 'json',
    method: 'POST',
    url: '/administrador/get_cities/',
    data: {
      pais: country,
      csrfmiddlewaretoken: csrf_token
    },
    success: function (data) {
      var opciones = '';
      var ciudades = JSON.parse(data);
      for (var i in ciudades) {
        if ((typeof (selected_city) != 'undefined') && (ciudades[i].id_ciudad == selected_city)) {
          opciones += '<option value="' + ciudades[i].id_ciudad + '" selected>' + ciudades[i].nombre_ciudad + '</option>';
        } else {
          opciones += '<option value="' + ciudades[i].id_ciudad + '">' + ciudades[i].nombre_ciudad + '</option>';
        }
      }
      $(elem_select).empty().append(opciones);
    }
  });
}