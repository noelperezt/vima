/**
 * Created by Johanna on 06/03/2017.
 */
var cod_prod, precio_prod,atributos;
var jsonProd = [];
$(function(){
    $("#id_id_proveedor").attr("title","Proveedor");
    $("#id_id_almacen").attr("title","Almacén");
    $(document).on("change","#color",function (){
        var color = $(this).val();
        var opciones="";
        $.each(atributos, function(indice, valor){
            if(valor.codigo_color == color){
                 $.each(valor.tallas, function(i,talla) {
                    opciones+='<option value="'+talla+'" >'+talla+'</option>';
                 });
            }
        });
        $("#talla","#form-caracteristicas-prod").html(opciones);
        $('.selectpicker').selectpicker('refresh');

    });
    $("#porcentaje_ganancia").on('change',function (){
        var valor = parseFloat($(this).val());
        var precio = parseFloat($("#precio_compra").val());
        var calcular = parseFloat(((precio*valor)/100) + precio);
        if(!isNaN(calcular)){
          $("#precio_venta").val(number_format(calcular,2,'.',','));
        }else{
            $("#precio_venta").val('');
        }

    });
    $("#precio_venta").on('change',function (){
        var valor = parseFloat($(this).val());
        var precio = parseFloat($("#precio_compra").val());
        var restar = parseFloat(valor - precio);
        var calcular = parseFloat(((restar*100)/precio));

        if(!isNaN(calcular)){
          $("#porcentaje_ganancia").val(number_format(calcular,2,'.',','));
        }else{
            $("#porcentaje_ganancia").val('');
        }


    });
    $("#add-caracteristicas").click(function (){
        var talla = $("#talla").val();
        var marca = $("#marca").val();
        var color = $("#color").val();
        var cant = $("#cant").val();
        var porcentaje = $("#porcentaje_ganancia").val();
        var precioVenta = $("#precio_venta").val();

        var nuevo = [];
        var caract = [];
        var cod_product = [];
        var colores = [];
        var Ncolores = [];
        var tallas = [];
        var size = [];
        var nombre_color ="";
        var cantidades = [];
        var caracteristicas = [];
        var precioVentas = [];
         var nombreProducto = $("#search_input").val();
         var precio = precio_prod;

          if(porcentaje  == ""  || precioVenta  == ""){
                alert("Por favor verifique el porcentaje de ganancia o el precio de venta no se han agregado");
                return false;
          }

         $("#color option").each(function () {
             var value = $(this).val();
             if(color == value){
                 nombre_color = $(this).attr('nombre');
             }
         });

        if(talla == null || talla == "" || color == "" || cant == ""){
            alert("Debe llenar todos los campos solicitados");
            return false;
        }
        for (var f in talla) {
            tallas.push(talla[f]);
            cantidades.push(parseInt(cant));
        }
        var count = Object.keys(jsonProd).length;

        if (count == 0){
            cod_product.push(cod_prod);
            colores.push(color);
            Ncolores.push(nombre_color);
             precioVentas.push(porcentaje,precioVenta);
            caracteristicas.push(tallas, cantidades, precioVentas);
            size.push(caracteristicas);
            caract.push(nombreProducto,precio);

            nuevo.push(cod_product,colores,size,caract,Ncolores,precioVentas);
            jsonProd.push(nuevo);
        }else{
            cod_product.push(cod_prod);
            colores.push(color);
            Ncolores.push(nombre_color);
            precioVentas.push(porcentaje,precioVenta);
            caracteristicas.push(tallas, cantidades, precioVentas);
            size.push(caracteristicas);
            caract.push(nombreProducto,precio);

            nuevo.push(cod_product,colores,size,caract,Ncolores,precioVentas);
            var existe = false;
            var col = [];
            var car = [];
            var n_color = [];
            for (var e in jsonProd) {
                var matrizP = jsonProd[e];
                var codigo_prod= matrizP[0];
                col = matrizP[1];
                n_color = matrizP[4];
                car = matrizP[2];
                if(codigo_prod[0] == cod_prod){
                     existe= true;
                        break;

                }
            }

            var colorprod = col.indexOf(color);
            if(existe == false){
                jsonProd.push(nuevo);

            }else{
                if(colorprod == '-1'){
                    col.push(color);
                    n_color.push(nombre_color);
                    car.push(caracteristicas);
                }else{
                   /* if(){

                    }*/
                    var tall=car[colorprod];
                    var caracte = tall[0];
                    var caraccant = tall[1];
                    for(var z in talla){
                        var arrayT = caracte.indexOf(talla[z]);
                        if(arrayT == '-1'){
                            caracte.push(talla[z]);
                            caraccant.push(parseInt(cant));
                        }else{
                            caraccant[arrayT] = parseInt(caraccant[arrayT])+parseInt(cant);
                        }
                    }
                }
            }
        }
        FilasTabla(jsonProd,precioVenta);
    });


    $(document).on('click',".glyphicon-trash",function () {
        var padre = $(this).parents("tr");
        var codigo = padre.attr('codigo');
        var color = padre.attr('color');

        var existe = false;
        var indice = 0;
        var col = [];
        var car = [];
        for (var e in jsonProd) {
            var matrizP = jsonProd[e];

            var codigo_prod= matrizP[0];
            if(codigo_prod[0] == codigo){
                col = matrizP[1];
                car = matrizP[2];
                indice = parseInt(e);
                 existe = true;
                break;
            }
        }
        if(existe === true){
             if(col.length > 1){
                var colorprod = col.indexOf(color);
                col.splice(colorprod,1);
                car.splice(colorprod,1);
                padre.remove();
            }else{
                 jsonProd.splice(indice,1);
                padre.remove();
             }
        }
         FilasTabla(jsonProd);

    });
});
function validarCampos() {
     var Proveedor = $('#id_id_proveedor').val();
     var alamcen = $('#id_id_almacen').val();
     var porcentaje = $('#porcentaje_ganancia').val();
     var precio_venta = $('#precio_venta').val();
     var length = $("#tbodyP tr").length;
    if(Proveedor == "" || alamcen == "" || length == 0){
        mostrarMensajes("Por favor verifique hay campos que son requeridos y no los ha llenado");
        return false;
    }else{
        return true;
    }
}
function mostrarCaracteristicas(codigo,descripcion, precio, atrr) {
    cod_prod=codigo;
    precio_prod=precio;
    $("#talla").html("");
    $("#color, #cant, #porcentaje_ganancia, #precio_venta").val("");
    $('.selectpicker').selectpicker('refresh');
    if(cod_prod!=codigo){
        mostrarMensajes("El códgo de producto ha cambiado verifique su porcentaje de ganacia es posible que no sea igual para éste nuevo producto");
        return false;
    }
    atributos = JSON.parse(Base64.decode(atrr));
    $("#search_input").val(descripcion);
    $("#precio_compra").val(precio);
    $(".results").css("display","none");
    llenarCaracteristicas(atributos);
    $('#form-caracteristicas-prod').modal('show');
}
function llenarCaracteristicas(atributos) {
    var opciones = "";
    $.each(atributos, function( index, value ) {
       opciones+='<option value="'+value.codigo_color+'" nombre="'+value.nombre_color+'" data-content="<div style=\'text-align: center;padding: 2px;border-radius: 4px;background-color:#'+value.codigo_color+';\'>'+value.nombre_color+'</div>"><div style="background-color:'+value.codigo_color+';">'+value.nombre_color+'</div></option>';
    });
    $("#color","#form-caracteristicas-prod").html(opciones);
    $('.selectpicker').selectpicker('refresh');

}
function Previsualizar() {
    proveedor=$('#id_id_proveedor').val();
    if($("#tbodyP tr").length > 0 && proveedor != ""){
        $.ajax({
            dataType : 'json',
            method : 'POST',
            url : '/compras/orden/previsualizar/',
            data : {
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
                proveedor : $('#id_id_proveedor').val()
            },
            success : function(data) {
                var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                var f=new Date();
                var fecha_pedido = f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();

                $(".modal-body","#form-previsualiza").find("div").html("");
                $.each(data,function(index,valor){
                    var Numero_orde = "";
                    if(typeof $("#orden").val() != "undefined"){
                            Numero_orde =  pad($("#orden").val(), 4);
                    }
                    
                    $(".modal-body","#form-previsualiza").find("#n_orden").html(Numero_orde);
                    $(".modal-body","#form-previsualiza").find("#rif_p").html(valor.fields.dni_rif_documento);
                    $(".modal-body","#form-previsualiza").find("#Proveedor").html(valor.fields.nombre_empresa);
                    $(".modal-body","#form-previsualiza").find("#telef").html(valor.fields.telefono);
                    $(".modal-body","#form-previsualiza").find("#direcc").html(valor.fields.direccion_fiscal);
                    $(".modal-body","#form-previsualiza").find("#fecha_pedido").html(fecha_pedido);

                });
               var $tabla = $("#tabla-productos").clone().appendTo($("#form-previsualiza").find("#contT"));
               $('#form-previsualiza').modal('show');

            }
        });
    }else{
        mostrarMensajes("No ha agregado las características del producto y/o ha seleccionado al proveedor, para previsualizar la orden compra");
    }

}
function pad(n, length) {
    var  n = n.toString();
    while(n.length < length)
         n = "0" + n;
    return n;
}
function FilasTabla(jsonProd,precioVenta){
    var contar = 0;
    var total = 0;
    var columnas = "";
    var indice = 0;
     var totales = 0;
    var totales_total =0;
    var totales_precioV =0;
    for (var i in jsonProd){
        var matrizP = jsonProd[i];
        var prod = matrizP[0];
        var col = matrizP[1];
        var car = matrizP[2];
        var caract = matrizP[3];

        for(var z in prod){
            var contar = 0;

            for(var c in col){
                contar=0;
                if(contar > 0){
                    columnas += '</tr>';
                }
                columnas += '<tr color="'+col[c]+'" codigo="'+prod[z]+'">';
                columnas += '<td>'+prod[z]+'</td>';
                columnas += '<td><div style="text-align: center;" class="nomb_prod">'+caract[0]+'</div>';
                columnas += '<div class="prod" style="margin: 0 auto;width: 68%;text-align: center;">';
                columnas += '<table id=tallaP style="text-align: center;width: 100%;">';
                    var tall = car[c];
                    var tallas = car[c][0];
                    var cantidades =car[c][1];
                    var porcentajeGanancia =car[c][2];

                    columnas += '<tr>';

                    for(var t in tallas){

                            var letra = tallas[t];
                            columnas += '<td>' + letra + '</td>';


                    }
                    columnas += '<tr/>';
                    columnas += '<tr>';
                    for(var an in cantidades){

                            var cantletra = cantidades[an];
                            if(!isNaN(cantletra)){
                                contar+= parseInt(cantletra);
                            }
                            columnas += '<td>' + cantletra + '</td>';

                    }
                    columnas += '<tr/>';

                columnas += '</table></div>';
                columnas += '</td>';
                columnas += '<td><div  style="background:#'+col[c]+';border-radius: 20px;font-family: AzoSansBold;vertical-align: bottom;padding: 4px 0 0 0;">'+col[c]+'</div></td>';
                 totales_total+=parseFloat(caract[1]*contar);
                total = number_format(parseFloat(caract[1]*contar),2,'.',',');
                totales_precioV +=parseFloat(porcentajeGanancia[1]);

                totales+=parseFloat(caract[1]);
                columnas += '<td>'+contar+'</td>';
                columnas += '<td><div class="precioUnitario">'+number_format(caract[1],2,'.',',')+'</div></td>';
                columnas += '<td><div class="precioVenta">'+number_format(porcentajeGanancia[0],2,'.',',')+'</div></td>';
                columnas += '<td><div class="precioVenta">'+number_format(porcentajeGanancia[1],2,'.',',')+'</div></td>';
                columnas += '<td><div class="precio_total">'+total+'</div></td>';
                columnas += '<td><div style="margin: 0 auto; width: 60%;cursor:pointer;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></div></td>';

                contar++;
            }
            columnas += '</tr>';
        }

    }
    $("#tbodyP").html("");
    $("#tbodyP").append(columnas);


    $("#TotalU").html(number_format(totales,2,'.',','));
    $("#TotatotalPrecio").html(number_format(totales_precioV,2,'.',','));
    $("#Totatotal").html(number_format(totales_total,2,'.',','));
    $("#talla").html("");
    $("#color, #cant, #porcentaje_ganancia, #precio_venta").val("");
    $('.selectpicker').selectpicker('refresh');

}
function crearJson(){
    var detalle=[];
    var total=0;
    for (var i in jsonProd) {
        var matrizP = jsonProd[i];
        var prod = matrizP[0];
        var col = matrizP[1];
        var Ncol = matrizP[4];
        var car = matrizP[2];
        var caract = matrizP[3];
        var porcentaje = matrizP[5];
        for (var z in prod) {
            for (var c in col) {
                var tallas = car[c][0];
                var cantidades =car[c][1]
                var precioV =car[c][2]
                for (var t in tallas) {
                    total++;
                    detalle.push({codigo_producto: prod[z],nombre_color:Ncol[c],color: col[c],talla: tallas[t],cantidad: cantidades[t],precio: caract[1], porcentaje: precioV[0], monto_venta: precioV[1]});
                }
            }
        }
    }

    var json = {
        cantidad_detalle : total,
        proveedor : $('#id_id_proveedor').val(),
        observacion: $('#id_observaciones').val(),
        almacen : $('#id_id_almacen').val(),
        usuario: $('#id_id_usuario').val(),
        estado : $('#estado').val(),
        detalle :detalle
    }
    return json;
}

