function __init()
{

    $('#search_input')
        .val('')
        .focus()
        .keyup(function(){

            if(!$.trim($(this).val()))
                $('.results .error').empty().hide();
        });

    var cache = {};
    $('#search_input').autocomplete({
        minLength: 2,
        select: function( event, ui ) {
            return false;
        },
        open: function() {
            $('.results .wrapper').html($(this).autocomplete("widget").html());
            $(this).autocomplete("widget").hide();
        },
        source: function( request, response ) {
            var Proveedor = $('#id_id_proveedor').val();

            if (cache[request.term]) {
                response(cache[request.term]);
                return;
            }

            if(Proveedor == ""){
                mostrarMensajes("Por favor seleccionar el proveedor y el porcentaje de ganancia");
                return false;
            }
            $.ajax({
                dataType : 'json',
                method : 'POST',
                url : '/compras/orden/search/',
                data : {
                    buscador : encodeURIComponent(request.term),
                    csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val(),
                    proveedor : $('#id_id_proveedor').val()
                },
                success : function(data) {
                    var prod = [];

                    for(var x in data)
                    {
                        prod.push({
                            id_producto : data[x].pk,
                            descripcion : data[x].fields['descripcion'],
                            precio_proveedor : data[x].fields['precio_proveedor'],
                            atributos : data[x].fields['atributos'],
                        });
                    }

                    cache[request.term] = prod;
                    response(prod);
                }
            });
        },
        response: function(event, ui) {

            if (ui.content.length === 0) {
                $(".results").css("display","block");

                $('.results .error').html('No se encontraron resultados').show();
                $('.results .wrapper').empty();
                setTimeout(function () {
                    $(".results").css("display","none");
                },2000);

            }else{
                $(".results").css("display","none");
                $('.results .error').empty().hide();
            }

        }
    }).autocomplete('instance')._renderItem = function(ul, item) {
        $(".ui-helper-hidden-accessible").remove();
        var condigo = str_pad(item.id_producto, 4, '0', 'STR_PAD_LEFT');
        $(".results").css("display","block");
        var attr=item.atributos;
        var carac = Base64.encode(attr);
        var user_tmpl = $('<div />')
                        .addClass('producto')
                        .append('<a  href="javascript: mostrarCaracteristicas(\''+condigo+'\',\''+item.descripcion+'\',\''+item.precio_proveedor+'\',\''+carac+'\');" />').find('a').addClass('descripcion').html(item.descripcion)
                        .parent();

        return $('<div></div>')
            .data('item.autocomplete', item)
            .append(user_tmpl)
            .appendTo(ul);

    };
}

$(document).ready(__init);