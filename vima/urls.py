"""vima URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from apps.administrador.views import cerrar_sesion
from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete


urlpatterns = [
    # url(r'^admin/', admin.site.urls), no se usara el admin de django
    url(r'^reset/password_reset', password_reset, 
        {'template_name':'clave_formulario.html',
        'email_template_name': 'clave_correo.html'}, 
        name='password_reset'), 
    url(r'^password_reset_done', password_reset_done, 
        {'template_name': 'clave_enviado.html'}, 
        name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm, 
        {'template_name': 'clave_confirmar.html'},
        name='password_reset_confirm'
        ),
    url(r'^reset/done', password_reset_complete, {'template_name': 'clave_cambiada.html'},
        name='password_reset_complete'),


    url(r'^administrador/', include('apps.administrador.urls', namespace='administrador')),
    url(r'^inventario/', include('apps.inventario.urls', namespace='inventario')),
    url(r'^compras/', include('apps.compras.urls', namespace='compras')),
    url(r'^cuentas_pagar/', include('apps.cuentas_pagar.urls', namespace='cuentas_pagar')),
    url(r'^cerrar_sesion/$', cerrar_sesion, name='cerrar_sesion'),

    url(r'^', include('apps.website.urls', namespace='website'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
